package net.minecraft;

import java.applet.Applet;
import java.applet.AppletStub;
import java.awt.BorderLayout;
import java.awt.Color;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import fr.aurum.MinecraftFrame;
import fr.aurum.update.AurumUpdater;

public class Launcher extends Applet
implements Runnable, AppletStub
{
	private static final long serialVersionUID = 1L;
	public Map<String, String> customParameters = new HashMap<String, String>();
	private AurumUpdater aurumUpdater;
	private Applet applet;
	private boolean active = false;
	private int context = 0;

	private MinecraftFrame frame = null;

	public static String serverIP = "";
	public static String serverPort = "";

	public boolean isActive()
	{
		if (this.context == 0) {
			this.context = -1;
			try {
				if (getAppletContext() != null) this.context = 1; 
			}
			catch (Exception localException) {
			}
		}
		if (this.context == -1) return this.active;
		return super.isActive();
	}

	public void init(String userName, String latestVersion, String downloadTicket, String sessionId)
	{
		this.customParameters.put("username", userName);
		this.customParameters.put("sessionid", sessionId);

		//this.gameUpdater = new GameUpdater(latestVersion, "minecraft.jar?user=" + userName + "&ticket=" + downloadTicket);
	}

	public boolean canPlayOffline() 
	{
		return false;
	}

	public void init() 
	{
		if (this.applet != null) 
		{
			this.applet.init();
			return;
		}
		init(getParameter("userName"), getParameter("latestVersion"), getParameter("downloadTicket"), getParameter("sessionId"));
	}

	public void start(MinecraftFrame frame, AurumUpdater updater) 
	{
		this.frame = frame;
		this.aurumUpdater = updater;
		this.frame.setVisible(false);
		this.frame.dispose();
		this.frame.setBackground(Color.white);
		this.frame.setUndecorated(false);
		this.frame.setVisible(true);
		if (this.applet != null) 
		{
			this.applet.start();
			return;
		}

		Thread t = new Thread() 
		{
			public void run() 
			{
				try {
					if (!Launcher.this.aurumUpdater.fatalError)
						Launcher.this.replace(Launcher.this.aurumUpdater.createApplet());
				}
				catch (ClassNotFoundException e)
				{
					e.printStackTrace();
				} catch (InstantiationException e) {
					e.printStackTrace();
				} catch (IllegalAccessException e) {
					e.printStackTrace();
				}
			}
		};
		t.setDaemon(true);
		t.start();

		t = new Thread() {
			public void run() {
				while (Launcher.this.applet == null) {
					Launcher.this.repaint();
					try {
						Thread.sleep(10L);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			}
		};
		t.setDaemon(true);
		t.start();
		this.customParameters.put("server", serverIP);
		this.customParameters.put("port", serverPort);
	}

	public void stop() {
		if (this.applet != null) {
			this.active = false;
			this.applet.stop();
			return;
		}
	}

	public void destroy() {
		/* 118 */     if (this.applet != null) {
			/* 119 */       this.applet.destroy();
			/* 120 */       return;
		}
	}

	public void replace(Applet applet) 
	{
		this.applet = applet;
		applet.setStub(this);
		applet.setSize(getWidth(), getHeight());

		setLayout(new BorderLayout());
		add(applet, "Center");

		applet.init();
		this.active = true;
		applet.start();
		validate();
	}

	public void run() {}

	public String getParameter(String name) 
	{
		String custom = (String)this.customParameters.get(name);
		if (custom != null) return custom; try
		{
			return super.getParameter(name);
		} catch (Exception e) {
			this.customParameters.put(name, null);
		}return null;
	}

	public void appletResize(int width, int height){}

	public URL getDocumentBase() 
	{
		try {
			return new URL("http://www.minecraft.net/game/");
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
		return null;
	}
}