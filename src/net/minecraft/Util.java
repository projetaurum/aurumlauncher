package net.minecraft;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.Proxy;
import java.net.URL;
import java.nio.charset.Charset;

import org.apache.commons.io.IOUtils;

public class Util
{
	private static File workDir = null;

	public static File getWorkingDirectory() 
	{
		if (workDir == null) workDir = getWorkingDirectory("projet-aurum");
		return workDir;
	}



	public static File getWorkingDirectory(String applicationName) 
	{
		String userHome = System.getProperty("user.home", ".");

		File workingDirectory;
		switch (getPlatform().ordinal()) 
		{
		case 1:
		case 2:
			workingDirectory = new File(System.getenv("APPDATA"), '.' + applicationName + '/');
			break;
		case 3:
			String applicationData = System.getenv("APPDATA");
			if (applicationData != null) workingDirectory = new File(applicationData, "." + applicationName + '/'); else
				workingDirectory = new File(userHome, '.' + applicationName + '/');
			break;
		case 4:
			workingDirectory = new File(userHome, "Library/Application Support/" + applicationName);
			break;
		default:
			workingDirectory = new File(userHome, applicationName + '/');
		}

		if ((!workingDirectory.exists()) && (!workingDirectory.mkdirs())) throw new RuntimeException("The working directory could not be created: " + workingDirectory);
		return workingDirectory;
	}

	public static OS getPlatform() 
	{
		String osName = System.getProperty("os.name").toLowerCase();

		if (osName.contains("win")) return OS.windows;
		if (osName.contains("mac")) return OS.macos;
		if (osName.contains("solaris")) return OS.solaris;
		if (osName.contains("sunos")) return OS.solaris;
		if (osName.contains("linux")) return OS.linux;
		if (osName.contains("unix")) return OS.linux;

		return OS.unknown;
	}



	public static String excutePost(String targetURL, String urlParameters)

	{

		/* 54 */    HttpURLConnection connection = null;

		try

		{

			/* 57 */      URL url = new URL(targetURL);

			/* 58 */      connection = (HttpURLConnection)url.openConnection();

			/* 59 */      connection.setRequestMethod("POST");

			/* 60 */      connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");



			/* 62 */      connection.setRequestProperty("Content-Length", Integer.toString(urlParameters.getBytes().length));

			/* 63 */      connection.setRequestProperty("Content-Language", "en-US");



			/* 65 */      connection.setUseCaches(false);

			/* 66 */      connection.setDoInput(true);

			/* 67 */      connection.setDoOutput(true);



			/* 70 */      DataOutputStream wr = new DataOutputStream(connection.getOutputStream());

			/* 71 */      wr.writeBytes(urlParameters);

			/* 72 */      wr.flush();

			/* 73 */      wr.close();



			/* 76 */      InputStream is = connection.getInputStream();

			/* 77 */      BufferedReader rd = new BufferedReader(new InputStreamReader(is));



			/* 79 */      StringBuffer response = new StringBuffer();

			String line;

			/* 80 */      while ((line = rd.readLine()) != null)

			{

				/* 81 */        response.append(line);

				/* 82 */        response.append('\r');

			}

			/* 84 */      rd.close();

			/* 85 */      String str1 = response.toString();

			return str1;

		}

		catch (Exception e)

		{

			/* 89 */      e.printStackTrace();

			return null;

		}

		finally

		{

			/* 94 */      if (connection != null)

				/* 95 */        connection.disconnect();

		}

	}

	public static enum OS
	{
		linux, solaris, windows, macos, unknown;
	}

	public static URL constantURL(final String input) 
	{
		try 
		{
			return new URL(input);
		}
		catch(final MalformedURLException e) 
		{
			throw new Error(e);
		}
	}

	public static String performPost(final URL url, final String parameters, final Proxy proxy, final String contentType, final boolean returnErrorPage) throws IOException 
	{
		final HttpURLConnection connection = (HttpURLConnection) url.openConnection(proxy);
		final byte[] paramAsBytes = parameters.getBytes(Charset.forName("UTF-8"));

		connection.setConnectTimeout(15000);
		connection.setReadTimeout(15000);
		connection.setRequestMethod("POST");
		connection.setRequestProperty("Content-Type", contentType + "; charset=utf-8");

		connection.setRequestProperty("Content-Length", "" + paramAsBytes.length);
		connection.setRequestProperty("Content-Language", "en-US");

		connection.setUseCaches(false);
		connection.setDoInput(true);
		connection.setDoOutput(true);

		final DataOutputStream writer = new DataOutputStream(connection.getOutputStream());
		writer.write(paramAsBytes);
		writer.flush();
		writer.close();

		InputStream stream = null;
		try {
			stream = connection.getInputStream();
		}
		catch(final IOException e) {
			if(returnErrorPage) {
				stream = connection.getErrorStream();

				if(stream == null)
					throw e;
			}
			else
				throw e;
		}

		return IOUtils.toString(stream);
	}
}