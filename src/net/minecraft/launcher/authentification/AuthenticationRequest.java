package net.minecraft.launcher.authentification;

import fr.aurum.auth.AuthManager;

public class AuthenticationRequest
{
    private final net.minecraft.launcher.authentification.Agent agent;
	private final String username;
	private final String password;
	private final String clientToken;

    public AuthenticationRequest(final AuthManager authenticationService, final String password) {
        agent = authenticationService.getAgent();
        username = authenticationService.getUsername();
        clientToken = authenticationService.getClientToken();
        this.password = password;
    }
}
