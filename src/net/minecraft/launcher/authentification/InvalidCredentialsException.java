package net.minecraft.launcher.authentification;

public class InvalidCredentialsException extends AuthenticationException {
    /**
	 * 
	 */
	private static final long serialVersionUID = -2906770750886494004L;

	public InvalidCredentialsException() {
    }

    public InvalidCredentialsException(final String message) {
        super(message);
    }

    public InvalidCredentialsException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public InvalidCredentialsException(final Throwable cause) {
        super(cause);
    }
}