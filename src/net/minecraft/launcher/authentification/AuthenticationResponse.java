package net.minecraft.launcher.authentification;

public class AuthenticationResponse extends Response
{
    private String accessToken;
    private String clientToken;

    public String getAccessToken() 
    {
        return accessToken;
    }

    public String getClientToken() 
    {
        return clientToken;
    }
}
