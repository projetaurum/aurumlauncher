package net.minecraft.launcher.authentification;

import fr.aurum.auth.AuthManager;


public class RefreshRequest 
{
	private final String clientToken;
	private final String accessToken;

    public RefreshRequest(final AuthManager authenticationService) 
    {
        this(authenticationService, null);
    }

    public RefreshRequest(final AuthManager authenticationService, String t) 
    {
        clientToken = authenticationService.getClientToken();
        accessToken = authenticationService.getAccessToken();
    }
}