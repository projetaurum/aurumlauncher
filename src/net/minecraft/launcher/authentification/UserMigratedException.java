package net.minecraft.launcher.authentification;

public class UserMigratedException extends InvalidCredentialsException {
    /**
	 * 
	 */
	private static final long serialVersionUID = -8925138945185173167L;

	public UserMigratedException() {
    }

    public UserMigratedException(final String message) {
        super(message);
    }

    public UserMigratedException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public UserMigratedException(final Throwable cause) {
        super(cause);
    }
}