package fr.aurum.dialogs;

import java.awt.Color;
import java.awt.Dimension;

import javax.swing.JDialog;

import fr.aurum.form.GlobalForm;

public class MusiqueDialog extends JDialog
{
	private static final long serialVersionUID = -997349814353364739L;

	private GlobalForm form;

	public MusiquePane musiquePane;

	public MusiqueDialog(GlobalForm form)
	{
		this.form = form;

		this.musiquePane = new MusiquePane(this.form);

		setLayout(null);
		setUndecorated(true);
		//this.setBackground(new Color(0,0,0,0));
		this.setFocusable(true);

		this.setPreferredSize(new Dimension(234, 480));

		this.musiquePane.setBounds(0, 0, 234, 480);
		this.add(musiquePane);
	}
}