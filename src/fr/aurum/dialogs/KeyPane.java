package fr.aurum.dialogs;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;

import javax.swing.JPanel;

import fr.aurum.form.GlobalForm;
import fr.aurum.utils.FileUtil;

public class KeyPane extends JPanel
{
	private GlobalForm form;
	
	private Image bgImage;
	
	public KeyPane(GlobalForm form)
	{
		this.form = form;
		
		this.setLayout(null);
		this.setBackground(new Color(0,0,0,0));
		this.setOpaque(false);
		this.setFocusable(true);
		this.setBorder(javax.swing.BorderFactory.createEmptyBorder());
		
		this.bgImage = FileUtil.registerImage("popups/auth.png");
	}
	
	public void paintComponent(Graphics g2)
	{
		g2.drawImage(this.bgImage, 0, 0, this.getWidth(), this.getHeight(), null);
	}
}
