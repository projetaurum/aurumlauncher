package fr.aurum.dialogs;

import java.awt.Dimension;

import javax.swing.JDialog;

import fr.aurum.form.GlobalForm;

public class KeyDialog extends JDialog
{
	private GlobalForm form;
	
	private KeyPane keyPane;
	
	public KeyDialog(GlobalForm form)
	{
		this.form = form;
		
		this.keyPane = new KeyPane(this.form);
		
		setLayout(null);
		setUndecorated(true);
		setFocusable(true);
		
		setPreferredSize(new Dimension(338,128));
		
		this.keyPane.setBounds(0, 0, 338, 128);
		this.add(keyPane);
	}
}