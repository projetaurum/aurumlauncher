package fr.aurum.dialogs;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;

import javax.swing.JButton;
import javax.swing.JPanel;

import javazoom.jl.player.advanced.AdvancedPlayer;
import net.minecraft.Util;
import fr.aurum.MinecraftFrame;
import fr.aurum.form.GlobalForm;
import fr.aurum.utils.DownloadUtil;
import fr.aurum.utils.FileUtil;

public class MusiquePane extends JPanel
{
	private static final long serialVersionUID = -997349814353364739L;

	private Image bgImage = null;

	private GlobalForm form = null;

	private JButton musiqueClose = new JButton();

	private JButton musiqueStart = new JButton();

	private JButton musiqueStop = new JButton();

	private boolean canPlayMusique = true;

	private String actualMusique = "";

	private AdvancedPlayer player;

	private String[] musiquesNames;

	public MusiquePane(final GlobalForm form)
	{
		this.form = form;

		setLayout(null);
		this.setBackground(new Color(0,0,0,0));
		this.setOpaque(false);
		this.setFocusable(true);
		this.setBorder(javax.swing.BorderFactory.createEmptyBorder());

		this.bgImage = FileUtil.registerImage("forms/player.png");

		//Musique Close Button
		this.musiqueClose.setBounds(10, 230, 45, 45);
		this.musiqueClose.setOpaque(false);
		this.musiqueClose.setFocusable(false);
		this.musiqueClose.setBorderPainted(false);
		this.musiqueClose.setContentAreaFilled(false);
		this.add(musiqueClose);

		//Musique Start
		this.musiqueStart.setBounds(67, 55, 35, 35);
		this.musiqueStart.setOpaque(false);
		this.musiqueStart.setFocusable(false);
		this.musiqueStart.setBorderPainted(false);
		this.musiqueStart.setContentAreaFilled(false);
		this.add(musiqueStart);

		//Musique Stop
		this.musiqueStop.setBounds(170, 55, 35, 35);
		this.musiqueStop.setOpaque(false);
		this.musiqueStop.setFocusable(false);
		this.musiqueStop.setBorderPainted(false);
		this.musiqueStop.setContentAreaFilled(false);
		this.add(musiqueStop);

		if(this.player == null)
			this.musiqueStop.setEnabled(false);

		this.musiqueClose.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent ae)
			{
				form.musique.setVisible(false);
				form.musiqueOpen = false;
			}
		});

		this.musiqueStart.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent ae)
			{
				launchPlaylist();
			}
		});

		this.musiqueStop.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent ae)
			{
				stopPlaylist();
			}
		});
	}

	public void stopPlaylist()
	{
		System.out.println("Arr�t forc� de la musique !");
		this.player.close();
		this.canPlayMusique = false;
	}

	public void launchPlaylist()
	{
		try
		{
			File musiqueData = new File(Util.getWorkingDirectory()+"/musiques/musData.aur");

			BufferedReader br = new BufferedReader(new FileReader(musiqueData));
			final int nbrMus = Integer.parseInt(br.readLine());

			File[] toread = new File[nbrMus];

			final String[] musNames = new String[nbrMus];
			final Integer[] musLength = new Integer[nbrMus];

			for(int i = 0; i<nbrMus; i++)
			{
				musNames[i] = br.readLine();
				musLength[i] = Integer.parseInt(br.readLine());
			}

			System.out.println("OK");
			for(int i = 0; i<nbrMus; i++)
				toread[i] = new File(Util.getWorkingDirectory()+"/musiques/"+musNames[i]);
			br.close();

			this.musiqueStop.setEnabled(true);

			playPlayer(toread);

		}catch(Exception e){e.printStackTrace();}
	}

	public void preparePlaylist()
	{
		try 
		{
			File musiqueDir = new File(Util.getWorkingDirectory()+"/musiques");

			if(!musiqueDir.exists())
			{   
				musiqueDir.mkdirs();
			}

			System.out.println("Recuperation des informations de la liste de musiques.");
			DownloadUtil.getFile(MinecraftFrame.globalURL+"musiques/musData.aur", Util.getWorkingDirectory()+"/musiques/");

			File musiqueData = new File(Util.getWorkingDirectory()+"/musiques/musData.aur");

			BufferedReader br = new BufferedReader(new FileReader(musiqueData));
			final int nbrMus = Integer.parseInt(br.readLine());

			final String[] musNames = new String[nbrMus];
			final Integer[] musLength = new Integer[nbrMus];

			for(int i = 0; i<nbrMus; i++)
			{
				musNames[i] = br.readLine();
				musLength[i] = Integer.parseInt(br.readLine());
			}

			this.musiquesNames = musNames;

			if(musiqueDir.listFiles().length < nbrMus)
			{
				Thread t = new Thread()
				{
					@Override
					public void run()
					{
						launchPlayListDownload(nbrMus, musNames);
					}
				};
				t.start();
			}

			br.close();

		}catch(Exception e){e.printStackTrace();}
	}

	public void launchPlayListDownload(int nbrMus, String[] musiqueName)
	{
		for(int i = 0; i<nbrMus;i++)
		{
			System.out.println("T�l�chargement de "+musiqueName[i]);

			DownloadUtil.getFile(MinecraftFrame.globalURL+"musiques/"+musiqueName[i], Util.getWorkingDirectory()+"/musiques/");
		}

		System.out.println("T�l�chargement achev�.");
	}

	public void closePlayer() { if (player != null) player.close(); }

	public void playPlayer(final File[] files) 
	{
		new Thread() 
		{
			@SuppressWarnings("deprecation")
			public void run() 
			{
				for(File file : files)
				{
					if(canPlayMusique)
					{
						try 
						{
							FileInputStream fis = new FileInputStream(file);
							BufferedInputStream bis = new BufferedInputStream(fis);
							player = new AdvancedPlayer(bis);

							System.out.println("Lecture de "+file.getName());
							actualMusique = file.getName();
							player.play();
						}
						catch (Exception e) { System.out.println(e); }
					}
				}
				musiqueStop.setEnabled(false);
				actualMusique = "";
				canPlayMusique = true;
				this.stop();
			}
		}.start();
	}

	public void update(Graphics g)
	{
		paint(g);
	}

	public void paintComponent(Graphics g2)
	{
		int w = getWidth();
		int h = getHeight();
		g2.drawImage(this.bgImage, 0, 0, w, h, null);

		g2.setFont(new Font("Arial", 1, 20));
		g2.setFont(new Font("Arial", 1, 12));

		g2.setColor(Color.black);

		if(this.musiquesNames != null && this.musiquesNames.length != 0)
		{
			int i = 0;
			for(String name : this.musiquesNames)
			{
				g2.drawString(name.replaceAll(".mp3", ""), 65, 120+(i*25));
				i=i+1;
			}
		}
	}
}