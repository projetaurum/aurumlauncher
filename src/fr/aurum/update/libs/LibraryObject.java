package fr.aurum.update.libs;

import java.net.MalformedURLException;
import java.net.URL;

public class LibraryObject 
{
	private String url;
	private String name;
	private long length;
	private String directory;

	public LibraryObject(String url, String name)
	{
		this.url = url;
		this.name = name;
		System.out.println(name+" Initialized");
		try {
			this.length = Libraries.getFileSize(new URL(url));
		} catch (MalformedURLException e) {e.printStackTrace();}
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public long getLength() {
		return length;
	}

	public void setLength(long length) {
		this.length = length;
	}

	public String getDirectory() {
		return directory;
	}

	public void setDirectory(String directory) {
		this.directory = directory;
	}
}