package fr.aurum.update.libs;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import javax.swing.JProgressBar;

import net.minecraft.Util;

import org.apache.commons.io.FileUtils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import fr.aurum.update.assets.FileDownloader;

public class Libraries 
{
	public static final Gson gson = new GsonBuilder().create();
	private final static ArrayList<FileDownloader> downloadlibraries = new ArrayList<FileDownloader>();

	public static final void searchLibraries(String jsonURL, String version, JProgressBar bar) throws IOException
	{
		ArrayList<String> libs = new ArrayList<String>();
		BufferedReader br = new BufferedReader(new FileReader(new File(Util.getWorkingDirectory()+"/version")));
		String read = br.readLine();
		while(read != null)
		{
			libs.add(read);
			read = br.readLine();
		}
		br.close();
		
		for(String str2 : libs)
		{
			String[] str = str2.split(":");
			String packagee = str[0];
			String name = str[1];
			String versionFile = str[2];

			File file = new File(Util.getWorkingDirectory()+"/libraries/", "/"+packagee.replace(".","/")+"/"+name+"/"+versionFile+"/"+name+"-"+versionFile+".jar");
			URL url = new URL("https://libraries.minecraft.net/"+packagee.replace(".","/")+"/"+name+"/"+versionFile+"/"+name+"-"+versionFile+".jar");
			long length = getFileSize(url);
			System.out.println(url.toString());
			if ((!file.isFile())
					|| (FileUtils.sizeOf(file) != length)) {

				FileDownloader downloadable = new FileDownloader(url,file,length);

				downloadlibraries.add(downloadable);
			}
		}
		
		long last = 0;
		for(FileDownloader dwl : downloadlibraries)
		{
			System.out.println(bar.getValue());
			dwl.downloadFile(last, bar);
			last = last+dwl.getFilesize();
		}
	}

	public static final int getFileSize(URL url) 
	{
		HttpURLConnection conn = null;
		try {
			conn = (HttpURLConnection) url.openConnection();
			conn.setRequestMethod("HEAD");
			conn.getInputStream();
			return conn.getContentLength();
		} catch (IOException e) {
			return -1;
		} finally {
			conn.disconnect();
		}
	}
	
	public static final long getMaximumSize()
	{
		long l = 0;
		for(FileDownloader dwl : downloadlibraries)
		{
			l = l+dwl.getFilesize();
		}
		return l;
	}
}