package fr.aurum.update;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashSet;

import net.minecraft.Launcher;
import net.minecraft.Util;
import fr.aurum.update.assets.AssetJob;
import fr.aurum.update.libs.LibraryObject;
import fr.aurum.MinecraftFrame;
import fr.aurum.utils.DownloadUtil;
import fr.aurum.utils.FileUtil;

public class VersionManager 
{
	public static final ArrayList<LibraryObject> checkLibrairies()
	{
		ArrayList<LibraryObject> libsToMaj = new ArrayList<LibraryObject>();
		
		try
		{
			if(!new File(Util.getWorkingDirectory()+"/temp/").exists())
				new File(Util.getWorkingDirectory()+"/temp/").mkdirs();
			
			DownloadUtil.getFile(MinecraftFrame.globalURL+"versions/LibsVersions.aur", Util.getWorkingDirectory()+"/temp/");
			
			File netVersion = new File(Util.getWorkingDirectory()+"/temp/LibsVersions.aur");
			BufferedReader netBr = new BufferedReader(new FileReader(netVersion));
			
			int nbrLibs = Integer.parseInt(netBr.readLine());
			String[] netLibsNames = new String[nbrLibs];
			
			libsToMaj.clear();
			
			for(int i = 0; i<nbrLibs; i++)
			{
				netLibsNames[i] = netBr.readLine().replace("[MCNET]", "");
				
				if(!isPresent(netLibsNames[i]))
				{
					String[] str = netLibsNames[i].split("/");
					libsToMaj.add(new LibraryObject(netLibsNames[i].replace("[LIB]", ""),str[str.length-1]));
				}
			}
			
			if(!new File(Util.getWorkingDirectory()+"/lib/").exists())
				new File(Util.getWorkingDirectory()+"/lib/").mkdirs();
			
			netBr.close();
			
			File[] actualFiles = new File(Util.getWorkingDirectory()+"/lib/").listFiles();
			
			for(File f : actualFiles)
			{
				if(isParasite(netLibsNames, f.getName()))
					f.delete();
			}
			
			netVersion.delete();
		}
		catch(Exception e){e.printStackTrace();}
		
		return libsToMaj;
	}
	
	public static final ArrayList<AssetJob> checkAssets()
	{
		ArrayList<AssetJob> assets = new ArrayList<AssetJob>();
		
		return assets;
	}
	
	public static final ArrayList<String> checkMods()
	{
		ArrayList<String> modsToMaj = new ArrayList<String>();
		try 
		{
			if(!new File(Util.getWorkingDirectory()+"/temp/").exists())
				new File(Util.getWorkingDirectory()+"/temp/").mkdirs();

			DownloadUtil.getFile(MinecraftFrame.globalURL+"versions/ModsVersion.aur", Util.getWorkingDirectory()+"/temp/");

			File netVersion = new File(Util.getWorkingDirectory()+"/temp/ModsVersion.aur");
			BufferedReader netBr = new BufferedReader(new FileReader(netVersion));

			int nbrMods = Integer.parseInt(netBr.readLine());
			Integer[] netNumberVersion = new Integer[nbrMods];
			String[] netModsNames = new String[nbrMods];
			
			modsToMaj.clear();
			
			for(int i = 0; i<nbrMods;i++)
			{
				netModsNames[i] = netBr.readLine();
				netNumberVersion[i] = Integer.parseInt(netBr.readLine());
				
				if(!isPresent(netModsNames[i]))
					modsToMaj.add(netModsNames[i]);
			}
			netBr.close();
			
			/*
			 * La partie des fichiers deja presents.
			 */
			
			if(!new File(Util.getWorkingDirectory()+"/versions/").exists() || !new File(Util.getWorkingDirectory()+"/mods/").exists()
					|| !new File(Util.getWorkingDirectory()+"/versions/ModsVersion.aur").exists())
			{
				new File(Util.getWorkingDirectory()+"/versions/").mkdirs();
				new File(Util.getWorkingDirectory()+"/mods/").mkdirs();
				for(String s : netModsNames)
				{
					if(!modsToMaj.contains(s))
						modsToMaj.add(s);
				}
				
				if(new File(Util.getWorkingDirectory()+"/versions/ModsVersion.aur").exists())
					new File(Util.getWorkingDirectory()+"/versions/ModsVersion.aur").delete();
				FileUtil.move(new File(Util.getWorkingDirectory()+"/temp/ModsVersion.aur"), new File(Util.getWorkingDirectory()+"/versions/ModsVersion.aur"));
				
				return modsToMaj;
			}

			File actualVersion = new File(Util.getWorkingDirectory()+"/versions/ModsVersion.aur");
			BufferedReader actBr = new BufferedReader(new FileReader(actualVersion));
			
			int nbrMods2 = Integer.parseInt(actBr.readLine());
			
			Integer[] actNumberVersion = new Integer[nbrMods2];
			String[] actModsNames = new String[nbrMods2];
			
			for(int i = 0; i<nbrMods2;i++)
			{
				actModsNames[i] = actBr.readLine();
				actNumberVersion[i] = Integer.parseInt(actBr.readLine());
				
				if(!isPresent(actModsNames[i]) && !modsToMaj.contains(actModsNames[i]))
					modsToMaj.add(actModsNames[i]);
				if(!contains(netModsNames, actModsNames[i]) && !modsToMaj.contains(actModsNames[i]))
					modsToMaj.add(actModsNames[i]);
				if(netNumberVersion[getIndex(netModsNames,actModsNames[i])] > actNumberVersion[i] && !modsToMaj.contains(actModsNames[i]))
					modsToMaj.add(actModsNames[i]);
			}
			actBr.close();
			
			if(new File(Util.getWorkingDirectory()+"/versions/ModsVersion.aur").exists())
				new File(Util.getWorkingDirectory()+"/versions/ModsVersion.aur").delete();
			FileUtil.move(new File(Util.getWorkingDirectory()+"/temp/ModsVersion.aur"), new File(Util.getWorkingDirectory()+"/versions/ModsVersion.aur"));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return modsToMaj;
	}
	
	public static HashSet<String> checkJars(URL[] urlList) 
	{
		HashSet<String> jars = new HashSet<String>();
		
		for(URL url : urlList)
		{
			if(url.getFile().contains("Aurum.jar"))
			{
				if(!isPresent("[JAR]Aurum.jar"))
					jars.add("[JAR]Aurum.jar");
			}
			else
			{
				if(!isPresent("[JAR]"+url.getFile().split("/")[2]) && !url.getFile().contains("natives"))
					jars.add("[JAR]"+url.getFile().split("/")[2]);
			}
		}
		
		DownloadUtil.getFile(MinecraftFrame.globalURL+"versions/JarsVersion.aur", Util.getWorkingDirectory()+"/temp/");
		
		File netVersion = new File(Util.getWorkingDirectory()+"/temp/JarsVersion.aur");
		File actVersion = new File(Util.getWorkingDirectory()+"/versions/JarsVersion.aur");
		
		if(!actVersion.exists())
		{
			for(URL url : urlList)
			{
				if(!url.getFile().contains("natives"))
					jars.add("[JAR]"+url.getFile().split("/")[2]);
			}
		}
		else
		{
			try 
			{
				BufferedReader netBr = new BufferedReader(new FileReader(netVersion));
				BufferedReader actBr = new BufferedReader(new FileReader(actVersion));
				
				ArrayList<String> netJars = new ArrayList<String>();
				ArrayList<Integer> netJarsV = new ArrayList<Integer>();
				
				int nbrJar = Integer.parseInt(netBr.readLine());
				
				for(int i = 0; i<nbrJar;i++)
				{
					netJars.add(netBr.readLine());
					netJarsV.add(Integer.parseInt(netBr.readLine()));
				}
				netBr.close();
				
				int actNbrJar = Integer.parseInt(actBr.readLine());
				
				for(int i =0; i<actNbrJar;i++)
				{
					String jar = actBr.readLine();
					Integer jarV = Integer.parseInt(actBr.readLine());
					
					if(netJarsV.get(netJars.indexOf(jar)) > jarV && !jars.contains(jar))
						jars.add(jar);
				}
				
				actBr.close();
			} catch (Exception e) 
			{e.printStackTrace();}
		}
		
		if(!jars.isEmpty())
			jars.add("[JAR]"+getNatives(urlList));
		
		FileUtil.move(new File(Util.getWorkingDirectory()+"/temp/JarsVersion.aur"), new File(Util.getWorkingDirectory()+"/versions/JarsVersion.aur"));
		return jars;
	}
	
	public static void setServerData()
	{
		if(!new File(Util.getWorkingDirectory()+"/temp/").exists())
			new File(Util.getWorkingDirectory()+"/temp/").mkdirs();
		
		DownloadUtil.getFile(MinecraftFrame.globalURL+"versions/serveur.aur", Util.getWorkingDirectory()+"/temp/");
		File serveur = new File(Util.getWorkingDirectory()+"/temp/serveur.aur");
		
		try 
		{
			BufferedReader br = new BufferedReader(new FileReader(serveur));
			
			Launcher.serverIP = br.readLine();
			Launcher.serverPort = br.readLine();
			
			br.close();
		} catch (Exception e) {e.printStackTrace();}
		
		serveur.delete();
	}
	
	public static final String getNatives(URL[] urlList)
	{
		for(URL url : urlList)
		{
			if(url.getFile().contains("natives"))
				return url.getFile().split("/")[2];
		}
		return "";
	}
	
	public static final boolean isPresent(String s)
	{
		if(s.startsWith("[MOD]"))
		{
			if(!new File(Util.getWorkingDirectory()+"/mods/"+s.replace("[MOD]", "")).exists())
				return false;
		}
		else if(s.startsWith("[LIB]"))
		{
			if(!new File(Util.getWorkingDirectory()+"/lib/"+s.replace("[LIB]", "")).exists())
				return false;
		}
		else if(s.startsWith("[JAR]"))
		{
			if(s.contains("Aurum.jar"))
			{
				if(!new File(Util.getWorkingDirectory()+"/bin/Aurum.jar").exists())
					return false;
			}
			if(!new File(Util.getWorkingDirectory()+"/bin/"+s.replace("[JAR]", "")).exists())
				return false;
		}
		return true;
	}
	
	public static final boolean isParasite(String[] s, String s2)
	{
		for(String s3 : s)
		{
			if(s3.replace("[LIB]", "").equals(s2))
				return false;
		}
		return true;
	}
	
	public static final boolean contains(String[] s, String s2)
	{
		for(String s3 : s)
		{
			if(s3.equals(s2))
				return true;
		}
		return false;
	}
	
	public static final int getIndex(String [] s, String s2)
	{
		for(int i = 0; i<s.length;i++)
		{
			if(s[i].equals(s2))
			{
				return i;
			}
		}
		return -1;
	}
}