package fr.aurum.update;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;

import net.minecraft.Util;
import fr.aurum.MinecraftFrame;
import fr.aurum.utils.DownloadUtil;
import fr.aurum.utils.FileUtil;

public class AutoUpdater 
{
	public static final int versionID = 05;
	public static final String versionName = "Baldr";

	public static final void updateLauncher() throws URISyntaxException, IOException
	{
		String launcherFile = "jar";

		/*if(Util.getPlatform().equals(Util.OS.windows))
			launcherFile = "exe";*/

		System.out.println("Téléchargement du nouveau launcher");
		DownloadUtil.getFile(MinecraftFrame.globalURL+"launcher/AurumLauncher."+launcherFile, Util.getWorkingDirectory()+"/temp/");
		System.out.println("Installation du nouveau launcher");
		File currentJar = new File(AutoUpdater.class.getProtectionDomain().getCodeSource().getLocation().toURI());
		currentJar.delete();
		FileUtil.move(new File(Util.getWorkingDirectory()+"/temp/AurumLauncher."+launcherFile), new File(currentJar.getPath().replace("jar", "")+launcherFile));
		System.out.println("Redemarrage");
		restartLauncher();
	}

	public static final boolean needUpdate()
	{
		try 
		{
			if(!new File(Util.getWorkingDirectory()+"/temp/").exists())
				new File(Util.getWorkingDirectory()+"/temp/").mkdirs();

			try
			{
				DownloadUtil.getFile(MinecraftFrame.globalURL+"versions/Launcher.aur", Util.getWorkingDirectory()+"/temp/");
			}catch(Exception e){System.err.println("Impossible d'accéder au serveur de versions du launcher !");return false;}
			File version = new File(Util.getWorkingDirectory()+"/temp/Launcher.aur");

			BufferedReader br;
			br = new BufferedReader(new FileReader(version));

		if(Integer.parseInt(br.readLine())>AutoUpdater.versionID)
		{
			br.close();
			version.delete();
			return true;
		}
		version.delete();
		br.close();
		} catch (FileNotFoundException e) {e.printStackTrace();} catch (IOException e) {e.printStackTrace();}
		return false;
	}

	public static final String getNewVersionName()
	{
		String versionName = "";
		try 
		{
			File version = new File(Util.getWorkingDirectory()+"/temp/Launcher.aur");
			BufferedReader br = new BufferedReader(new FileReader(version));

			br.readLine();
			versionName = br.readLine();
			br.close();
		} catch (Exception e) {e.printStackTrace();}
		return versionName;
	}

	public static final void restartLauncher() throws URISyntaxException, IOException
	{
		final String javaBin = System.getProperty("java.home") + File.separator + "bin" + File.separator + "java";
		final File currentJar = new File(AutoUpdater.class.getProtectionDomain().getCodeSource().getLocation().toURI());

		System.out.println(currentJar.getPath());
		
		/*if(!currentJar.getName().endsWith(".jar") || !currentJar.getName().endsWith(".exe"))
			return;*/

		final ArrayList<String> command = new ArrayList<String>();
		command.add(javaBin);
		command.add("-jar");
		command.add(currentJar.getPath());

		final ProcessBuilder builder = new ProcessBuilder(command);
		System.out.println("Start new launcher");
		builder.start();
		System.out.println("Exit");
		System.exit(0);
	}
}