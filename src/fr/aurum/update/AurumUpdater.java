package fr.aurum.update;

import java.applet.Applet;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FilePermission;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.net.JarURLConnection;
import java.net.SocketPermission;
import java.net.URL;
import java.net.URLClassLoader;
import java.security.AccessController;
import java.security.CodeSource;
import java.security.PermissionCollection;
import java.security.PrivilegedExceptionAction;
import java.security.SecureClassLoader;
import java.security.cert.Certificate;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.StringTokenizer;
import java.util.Vector;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.jar.JarOutputStream;
import java.util.jar.Pack200;

import net.minecraft.Launcher;
import net.minecraft.Util;
import fr.aurum.update.libs.LibraryObject;
import fr.aurum.MinecraftFrame;
import fr.aurum.utils.DownloadUtil;

public class AurumUpdater extends Thread
{
	private static ClassLoader classLoader;

	private Launcher launcher;
	private MinecraftFrame frame;

	private String[] values;
	private URL[] urlList;

	public String fatalErrorDescription;

	public boolean fatalError = false;
	private boolean lzmaSupported = false;
	private boolean pack200Supported = false;
	private static boolean natives_loaded = false;

	private String mainGameUrl;

	public AurumUpdater(Launcher launcher, MinecraftFrame frame, String values[], String mainGameUrl)
	{
		this.launcher = launcher;
		this.frame = frame;
		this.values = values;
		this.mainGameUrl = mainGameUrl;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public void run() 
	{	
		this.frame.globalForm.switchOnglet(2);
		this.frame.globalForm.lockDownload();

		VersionManager.setServerData();

		Thread t = new Thread() 
		{
			public void run() 
			{
				while (true) 
				{
					frame.globalForm.download.repaint();
					try 
					{
						Thread.sleep(20L);
					} catch (InterruptedException e) { e.printStackTrace(); }
				}
			}
		};
		t.start();

		String path = null;
		try {
			path = (String)AccessController.doPrivileged(new PrivilegedExceptionAction() 
			{
				public Object run() throws Exception 
				{
					return Util.getWorkingDirectory() + File.separator + "bin" + File.separator;
				}
			});
			File dir = new File(path);

			if (!dir.exists()) {dir.mkdirs();}

			System.out.println("#############");
			System.out.println("#  V.CHECK  #");
			System.out.println("#############");
			
			this.frame.globalForm.download.actualThread = "Version check";
			ArrayList<String> downloads = VersionManager.checkMods();
			ArrayList<LibraryObject> libraries = VersionManager.checkLibrairies();
			loadJarURLs();
			HashSet<String> downloads3 = VersionManager.checkJars(this.urlList);

			this.frame.globalForm.download.actualThread = "Mods list add";
			for(String s : downloads)
			{
				this.frame.globalForm.download.totalLength = 
						this.frame.globalForm.download.totalLength+DownloadUtil.getLength(MinecraftFrame.globalURL+"mods/"+s.replace("[MOD]", ""));
				this.frame.globalForm.download.addDownload(s.replace("[MOD]", ""), "MOD");
			}
			this.frame.globalForm.download.actualThread = "Libs list add";
			for(LibraryObject lib : libraries)
			{
				this.frame.globalForm.download.totalLength = 
						this.frame.globalForm.download.totalLength+lib.getLength();
				this.frame.globalForm.download.addDownload(lib.getName(), "LIB");
			}
			this.frame.globalForm.download.actualThread = "Jars list add";
			for(String s3 : downloads3)
			{
				if(s3.contains("Aurum.jar"))
				{
					this.frame.globalForm.download.totalLength = 
							this.frame.globalForm.download.totalLength+DownloadUtil.getLength(MinecraftFrame.globalURL+"jars/Aurum.jar");
					this.frame.globalForm.download.addDownload("Aurum.jar", "JAR");
				}
				else
				{
					this.frame.globalForm.download.totalLength = 
							this.frame.globalForm.download.totalLength+DownloadUtil.getLength("http://s3.amazonaws.com/MinecraftDownload/"+s3.replace("[JAR]", ""));
					this.frame.globalForm.download.addDownload(s3.replace("[JAR]", ""), "JAR");
				}
			}

			System.out.println(this.frame.globalForm.download.mods);
			System.out.println(this.frame.globalForm.download.libs);
			System.out.println(this.frame.globalForm.download.jars);
			
			System.out.println("#############");
			System.out.println("#   MODS    #");
			System.out.println("#############");
			
			this.frame.globalForm.download.actualThread = "Mods download";
			for(String s : downloads)
			{
				System.out.println("Telechargement de "+s);
				this.frame.globalForm.download.setActualFile(s.replace("[MOD]", ""));
				new DownloadUtil(MinecraftFrame.globalURL+"mods/"+s.replace("[MOD]", ""), Util.getWorkingDirectory()+"/mods/", this.frame.globalForm.download, "MOD").run();
			}

			System.out.println("#############");
			System.out.println("#Librairies #");
			System.out.println("#############");

			this.frame.globalForm.download.actualThread = "Libraries download";
			for(LibraryObject lib : libraries)
			{
				System.out.println("Telechargement de "+lib.getName());
				this.frame.globalForm.download.setActualFile(lib.getName());
				new DownloadUtil(lib.getUrl(), Util.getWorkingDirectory()+"/lib/", this.frame.globalForm.download, "LIB").run();
			}

			System.out.println("#############");
			System.out.println("#   Jars    #");
			System.out.println("#############");

			this.frame.globalForm.download.actualThread = "Jars download";
			for(String s : downloads3)
			{
				if(s.contains("Aurum.jar"))
				{
					System.out.println("T�l�chargement de Aurum.jar");
					this.frame.globalForm.download.setActualFile("Aurum.jar");
					new DownloadUtil(MinecraftFrame.globalURL+"jars/Aurum.jar", Util.getWorkingDirectory()+"/bin/", this.frame.globalForm.download, "JAR").run();
				}
				else
				{
					System.out.println("T�l�chargement de "+s);
					this.frame.globalForm.download.setActualFile(s.replace("[JAR]", ""));
					new DownloadUtil("http://s3.amazonaws.com/MinecraftDownload/"+s.replace("[JAR]", ""), Util.getWorkingDirectory()+"/bin/", this.frame.globalForm.download, "JAR").run();
				}
			}

			if(!downloads3.isEmpty())
			{
				this.frame.globalForm.download.actualThread = "Jars extract";
				extractJars(path);
				extractNatives(path);
			}

			System.out.println("#############");
			System.out.println("#  Launch   #");
			System.out.println("#############");

			this.frame.globalForm.download.actualThread = "Launch Game";
			updateClassPath(dir);
			launchGame();
		} catch (Exception e) {e.printStackTrace();}
	}

	public void launchGame()
	{
		this.launcher = new Launcher();
		this.launcher.customParameters.put("userName", values[2].trim());
		this.launcher.customParameters.put("latestVersion", values[0].trim());
		this.launcher.customParameters.put("downloadTicket", values[1].trim());
		this.launcher.customParameters.put("sessionId", values[3].trim());
		this.launcher.init();

		frame.removeAll();
		frame.add(this.launcher, "Center");
		frame.validate();
		this.launcher.start(frame, this);
		frame.loginForm = null;
		frame.setTitle("Minecraft");
	}

	protected void loadJarURLs() throws Exception 
	{
		String jarList = "lwjgl.jar, jinput.jar, lwjgl_util.jar, "+this.mainGameUrl;
		jarList = trimExtensionByCapabilities(jarList);

		StringTokenizer jar = new StringTokenizer(jarList, ", ");
		int jarCount = jar.countTokens() + 1;

		this.urlList = new URL[jarCount];

		URL path = new URL("http://s3.amazonaws.com/MinecraftDownload/");

		for (int i = 0; i < jarCount - 1; i++) 
		{
			String nextToken = jar.nextToken();
			URL oldpath = path;
			if(nextToken.indexOf("craft.jar") >= 0)
				path = new URL("http://projet-aurum.fr/launcher/Aurum.jar");
			if(nextToken.indexOf("craft.jar") >= 0)
			{
				this.urlList[i] = new URL(path, nextToken.replaceAll("minecraft.jar", "Aurum.jar"));
			}
			else
				this.urlList[i] = new URL(path, nextToken);
			if(nextToken.indexOf("craft.jar") >= 0)
				path = oldpath;
		}

		String osName = System.getProperty("os.name");
		String nativeJar = null;

		if (osName.startsWith("Win"))
			nativeJar = "windows_natives.jar.lzma";
		else if (osName.startsWith("Linux"))
			nativeJar = "linux_natives.jar.lzma";
		else if (osName.startsWith("Mac"))
			nativeJar = "macosx_natives.jar.lzma";
		else if ((osName.startsWith("Solaris")) || (osName.startsWith("SunOS")))
			nativeJar = "solaris_natives.jar.lzma";
		else {
			fatalErrorOccured("OS (" + osName + ") not supported", null);
		}

		if (nativeJar == null) {
			fatalErrorOccured("no lwjgl natives files found", null);
		} else {
			nativeJar = trimExtensionByCapabilities(nativeJar);
			this.urlList[(jarCount - 1)] = new URL(path, nativeJar);
		}
	}

	public Applet createApplet() throws ClassNotFoundException, InstantiationException, IllegalAccessException
	{
		Class<?> appletClass = classLoader.loadClass("net.minecraft.client.MinecraftApplet");
		return (Applet)appletClass.newInstance();
	}

	protected void updateClassPath(File dir) throws Exception
	{
		URL[] urls = new URL[this.urlList.length];
		for (int i = 0; i < this.urlList.length; i++) 
		{
			urls[i] = new File(dir, getJarName(this.urlList[i])).toURI().toURL();
		}

		if (classLoader == null) 
		{
			classLoader = new URLClassLoader(urls) 
			{
				protected PermissionCollection getPermissions(CodeSource codesource) 
				{
					PermissionCollection perms = null;
					try
					{
						Method method = SecureClassLoader.class.getDeclaredMethod("getPermissions", new Class[] { CodeSource.class });
						method.setAccessible(true);
						perms = (PermissionCollection)method.invoke(getClass().getClassLoader(), new Object[] { codesource });

						String host = "www.minecraft.net";

						if ((host != null) && (host.length() > 0))
							perms.add(new SocketPermission(host, "connect,accept"));
						else 
							codesource.getLocation().getProtocol().equals("file");

						perms.add(new FilePermission("<<ALL FILES>>", "read"));
					}
					catch (Exception e) {e.printStackTrace();}

					return perms;
				}
			};
		}
		String path = dir.getAbsolutePath();
		if (!path.endsWith(File.separator)) path = path + File.separator;
		unloadNatives(path);

		System.setProperty("org.lwjgl.librarypath", path + "natives");
		System.setProperty("net.java.games.input.librarypath", path + "natives");

		natives_loaded = true;
	}

	private void unloadNatives(String nativePath)
	{
		if (!natives_loaded) {
			return;
		}
		try
		{
			Field field = ClassLoader.class.getDeclaredField("loadedLibraryNames");
			field.setAccessible(true);
			Vector<?> libs = (Vector<?>)field.get(getClass().getClassLoader());

			String path = new File(nativePath).getCanonicalPath();

			for (int i = 0; i < libs.size(); i++) {
				String s = (String)libs.get(i);

				if (s.startsWith(path)) {
					libs.remove(i);
					i--;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	protected void fatalErrorOccured(String error, Exception e) 
	{
		e.printStackTrace();
		this.fatalError = true;
		this.fatalErrorDescription = ("Fatal error occured ! : " + error);
		System.out.println(this.fatalErrorDescription);
		if (e != null)
			System.out.println(generateStacktrace(e));
	}

	private String generateStacktrace(Exception exception) 
	{
		Writer result = new StringWriter();
		PrintWriter printWriter = new PrintWriter(result);
		exception.printStackTrace(printWriter);
		return result.toString();
	}

	protected String getJarName(URL url)
	{
		String fileName = url.getFile();

		if (fileName.contains("?")) 
		{
			fileName = fileName.substring(0, fileName.indexOf("?"));
		}
		if (fileName.endsWith(".pack.lzma"))
			fileName = fileName.replaceAll(".pack.lzma", "");
		else if (fileName.endsWith(".pack"))
			fileName = fileName.replaceAll(".pack", "");
		else if (fileName.endsWith(".lzma")) {
			fileName = fileName.replaceAll(".lzma", "");
		}

		return fileName.substring(fileName.lastIndexOf('/') + 1);
	}

	protected String trimExtensionByCapabilities(String file)
	{
		if (!this.pack200Supported) 
			file = file.replaceAll(".pack", "");
		if (!this.lzmaSupported) 
			file = file.replaceAll(".lzma", "");
		return file;
	}

	protected void extractNatives(String path) throws Exception
	{
		String nativeJar = getJarName(this.urlList[(this.urlList.length - 1)]);
		Certificate[] certificate = Launcher.class.getProtectionDomain().getCodeSource().getCertificates();

		if (certificate == null) 
		{
			URL location = Launcher.class.getProtectionDomain().getCodeSource().getLocation();

			JarURLConnection jurl = (JarURLConnection)new URL("jar:" + location.toString() + "!/net/minecraft/Launcher.class").openConnection();
			jurl.setDefaultUseCaches(true);
			try 
			{
				certificate = jurl.getCertificates();
			}
			catch (Exception localException){}
		}
		File nativeFolder = new File(path + "natives");
		if (!nativeFolder.exists()) {
			nativeFolder.mkdir();
		}

		JarFile jarFile = new JarFile(path + nativeJar, true);
		Enumeration<?> entities = jarFile.entries();

		while (entities.hasMoreElements()) 
		{
			JarEntry entry = (JarEntry)entities.nextElement();
			if ((entry.isDirectory()) || (entry.getName().indexOf('/') != -1)) 
				continue;
		}

		entities = jarFile.entries();

		while (entities.hasMoreElements()) 
		{
			JarEntry entry = (JarEntry)entities.nextElement();

			if ((entry.isDirectory()) || (entry.getName().indexOf('/') != -1))
			{
				continue;
			}
			File f = new File(path + "natives" + File.separator + entry.getName());
			if ((f.exists()) &&
					(!f.delete()))
			{
				continue;
			}

			InputStream in = jarFile.getInputStream(jarFile.getEntry(entry.getName()));
			OutputStream out = new FileOutputStream(path + "natives" + File.separator + entry.getName());

			byte[] buffer = new byte[65536];
			int bufferSize;
			while ((bufferSize = in.read(buffer, 0, buffer.length)) != -1)
			{
				out.write(buffer, 0, bufferSize);
			}

			validateCertificateChain(certificate, entry.getCertificates());

			in.close();
			out.close();
		}
		jarFile.close();

		File f = new File(path + nativeJar);
		f.delete();
	}

	protected static void validateCertificateChain(Certificate[] ownCerts, Certificate[] native_certs) throws Exception
	{
		if (ownCerts == null) return;
		if (native_certs == null) throw new Exception("Unable to validate certificate chain. Native entry did not have a certificate chain at all");

		if (ownCerts.length != native_certs.length) throw new Exception("Unable to validate certificate chain. Chain differs in length [" + ownCerts.length + " vs " + native_certs.length + "]");

		for (int i = 0; i < ownCerts.length; i++)
			if (!ownCerts[i].equals(native_certs[i]))
				throw new Exception("Certificate mismatch: " + ownCerts[i] + " != " + native_certs[i]);
	}

	protected void extractJars(String path) throws Exception
	{
		for (int i = 0; i < this.urlList.length; i++) 
		{
			String filename = getFileName(this.urlList[i]);

			if (filename.endsWith(".pack.lzma")) 
			{
				extractLZMA(path + filename, path + filename.replaceAll(".lzma", ""));
				extractPack(path + filename.replaceAll(".lzma", ""), path + filename.replaceAll(".pack.lzma", ""));
			} 
			else if (filename.endsWith(".pack")) 
				extractPack(path + filename, path + filename.replace(".pack", ""));
			else if (filename.endsWith(".lzma")) 
				extractLZMA(path + filename, path + filename.replace(".lzma", ""));
		}
	}

	protected String getFileName(URL url) 
	{
		String fileName = url.getFile();
		if (fileName.contains("?")) 
			fileName = fileName.substring(0, fileName.indexOf("?"));
		return fileName.substring(fileName.lastIndexOf('/') + 1);
	}

	protected void extractLZMA(String in, String out) throws Exception
	{
		File f = new File(in);
		FileInputStream fileInputHandle = new FileInputStream(f);

		Class<?> clazz = Class.forName("LZMA.LzmaInputStream");
		Constructor<?> constructor = clazz.getDeclaredConstructor(new Class[] { InputStream.class });
		InputStream inputHandle = (InputStream)constructor.newInstance(new Object[] { fileInputHandle });

		OutputStream outputHandle = new FileOutputStream(out);

		byte[] buffer = new byte[16384];

		int ret = inputHandle.read(buffer);
		while (ret >= 1) 
		{
			outputHandle.write(buffer, 0, ret);
			ret = inputHandle.read(buffer);
		}

		inputHandle.close();
		outputHandle.close();

		outputHandle = null;
		inputHandle = null;

		f.delete();
	}

	protected void extractPack(String in, String out) throws Exception
	{
		File f = new File(in);
		FileOutputStream fostream = new FileOutputStream(out);
		JarOutputStream jostream = new JarOutputStream(fostream);

		Pack200.Unpacker unpacker = Pack200.newUnpacker();
		unpacker.unpack(f, jostream);
		jostream.close();

		f.delete();
	}
}