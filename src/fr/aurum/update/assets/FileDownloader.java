package fr.aurum.update.assets;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import javax.swing.JProgressBar;

import org.apache.commons.io.FileUtils;


public class FileDownloader
{
	private final URL url;
	private final File target;
	private final long filesize;

	public FileDownloader(URL u, File f) {
		this.url = u;
		this.target = f;
		this.filesize = tryGetFileSize(u);
	}

	public FileDownloader(URL u, File f, long size) {
		this.url = u;
		this.target = f;
		this.filesize = size;
	}

	private int tryGetFileSize(URL url) {
		HttpURLConnection conn = null;
		try {
			conn = (HttpURLConnection) url.openConnection();
			conn.setRequestMethod("HEAD");
			conn.getInputStream();
			return conn.getContentLength();
		} catch (IOException e) {
			return -1;
		} finally {
			conn.disconnect();
		}
	}
	
	public void downloadFile(long last, JProgressBar bar) 
	{
		long actual = 0;
		boolean needupdate = false;
		String fileName = target.getName();
		if (!target.exists()) 
		{
			needupdate = true;

			try 
			{
				target.getParentFile().mkdirs();
				target.createNewFile();

			} catch (IOException ex) {target.delete();}
		}
		else 
		{
			long lenght = FileUtils.sizeOf(target);
			if (!(lenght == filesize)) 
			{
				needupdate = true;
			}
		}

		if (needupdate) 
		{
			InputStream in = null;
			FileOutputStream fout = null;
			try {
				in = url.openStream();
				fout = new FileOutputStream(target);

				byte data[] = new byte[1024];
				int count;
				while ((count = in.read(data, 0, 1024)) != -1) 
				{
					actual = actual+count;
					bar.setValue((int) (last+actual));
					fout.write(data, 0, count);
				}
			} catch (IOException e) {System.out.println("Cannot download file : " + fileName);target.delete();} 
			finally 
			{
				if (in != null) 
				{
					try 
					{
						in.close();
					} catch (IOException e) {target.delete();}
				}
				if (fout != null) 
				{
					try 
					{
						fout.close();
					} catch (IOException e) {target.delete();}
				}
				System.out.println("File " + fileName + " "+filesize+" octets downloaded successfully");
			}
		}
	}

	public void downloadFile() 
	{
		boolean needupdate = false;
		String fileName = target.getName();
		if (!target.exists()) 
		{
			needupdate = true;

			try 
			{
				target.getParentFile().mkdirs();
				target.createNewFile();

			} catch (IOException ex) {target.delete();}
		}
		else 
		{
			long lenght = FileUtils.sizeOf(target);
			if (!(lenght == filesize)) 
			{
				needupdate = true;
			}
		}

		if (needupdate) 
		{
			InputStream in = null;
			FileOutputStream fout = null;
			try {
				in = url.openStream();
				fout = new FileOutputStream(target);

				byte data[] = new byte[1024];
				int count;
				while ((count = in.read(data, 0, 1024)) != -1) 
				{
					fout.write(data, 0, count);
				}
			} catch (IOException e) {System.out.println("Cannot download file : " + fileName);target.delete();} 
			finally 
			{
				if (in != null) 
				{
					try 
					{
						in.close();
					} catch (IOException e) {target.delete();}
				}
				if (fout != null) 
				{
					try 
					{
						fout.close();
					} catch (IOException e) {target.delete();}
				}
				System.out.println("File " + fileName + " "+filesize+" octets downloaded successfully");
			}
		}
	}

	public URL getUrl() {
		return url;
	}

	public File getTarget() {
		return target;
	}

	public long getFilesize() {
		return filesize;
	}
}