package fr.aurum.update.assets;

public class AssetJob 
{
	private long size;
	private String virtualName;
	private String hash;
	
	public AssetJob(String asset, String virtual)
	{
		this.hash = asset;
		this.virtualName = virtual;
	}

	public String getVirtualName() {
		return virtualName;
	}

	public void setVirtualName(String virtualName) {
		this.virtualName = virtualName;
	}

	public long getSize() {
		return size;
	}

	public void setSize(long size) {
		this.size = size;
	}

	public String getHash() {
		return hash;
	}

	public void setHash(String hash) {
		this.hash = hash;
	}
}