package fr.aurum.update.assets;

import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

public class AssetIndex 
{
	public static final String DEFAULT_ASSET_NAME = "legacy";
	private final Map<String, AssetJob> objects;
	private boolean virtual;

	public AssetIndex() {
		this.objects = new LinkedHashMap<String, AssetJob>();
	}

	public Map<String, AssetJob> getFileMap() {
		return this.objects;
	}

	public Set<AssetJob> getUniqueObjects() {
		return new HashSet<AssetJob>(this.objects.values());
	}

	public boolean isVirtual() {
		return this.virtual;
	}


}