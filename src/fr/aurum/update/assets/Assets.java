package fr.aurum.update.assets;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.Map.Entry;

import net.minecraft.Util;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;

import com.google.gson.Gson;

public class Assets 
{
	public static final Gson gson = new Gson();

	public static final ArrayList<AssetJob> assets = new ArrayList<AssetJob>();

	private final static ArrayList<FileDownloader> downloadassets = new ArrayList<FileDownloader>();

	public static final void addAsset(String asset, String virtual)
	{
		assets.add(new AssetJob(asset, virtual));
	}

	public static final void removeAsset(String asset)
	{
		for(AssetJob assetJob : assets)
		{
			if(assetJob.getHash().equals(asset))
				assets.remove(assetJob);
		}
	}

	public static final void printlnAllAssets()
	{
		int i = 0;
		for(AssetJob asset : assets)
		{
			System.out.println("["+i+"] Asset "+asset.getVirtualName()+" | Object : "+asset.getHash()+" | Dir : "+asset.getHash().substring(0, 1)+" | Length : "+asset.getSize());
			i = i+1;
		}
	}

	public static final void searchAssets(String jsonURL, String version) throws IOException
	{
		AssetIndex index;
		InputStream inputStream = null;
		URL indexUrl = new URL(jsonURL);
		inputStream = indexUrl.openConnection()
				.getInputStream();
		String json = IOUtils.toString(inputStream);
		File objectsFolder = new File(Util.getWorkingDirectory()+"/assets/");

		FileUtils.writeStringToFile(new File(Util.getWorkingDirectory()+"/cache/"+version+".json"), json);

		index = gson.fromJson(json, AssetIndex.class);

		for (AssetJob job : index
				.getUniqueObjects()) {

			String filename = job.getHash().substring(0, 2)
					+ "/" + job.getHash();

			File file = new File(objectsFolder, filename);
			if ((!file.isFile())
					|| (FileUtils.sizeOf(file) != job.getSize())) {
				FileDownloader downloadable = new FileDownloader(new URL(
						"http://resources.download.minecraft.net/"
								+ filename), file,
								job.getSize());

				downloadassets.add(downloadable);
			}
		}
		
		downloadAssets(index);
	}

	public static final void downloadAssets(AssetIndex index) 
	{
		String indexName = "legacy";
		
		long totalLength = 0;
		for(FileDownloader todown : downloadassets)
		{
			totalLength = totalLength+todown.getFilesize();
			System.out.println("File : "+todown.getFilesize());
		}
		System.out.println("Total : "+(float)totalLength/1000000);
		for (FileDownloader todown : downloadassets) 
		{
			todown.downloadFile();
		}
		if(indexName.equals("legacy"))
		{
			for (Entry<String, AssetJob> entry : index.getFileMap().entrySet()) 
			{
				File target = new File(Util.getWorkingDirectory()+"/assets/", entry.getKey());
				File original = new File(new File(Util.getWorkingDirectory()+"/assets/legacy/",
						entry.getValue().getHash()
						.substring(0, 2)),
						entry.getValue().getHash());
				if (!target.isFile()) 
				{
					try 
					{
						FileUtils.copyFile(original, target, false);
					} catch (IOException e) {}
				} 
			}
		}
	}
}