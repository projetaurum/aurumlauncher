package fr.aurum.form;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.text.NumberFormat;
import java.util.HashMap;

import javax.imageio.ImageIO;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JProgressBar;

public class DownloadForm extends JPanel
{
	private static final long serialVersionUID = 7272314012660317633L;

	public JButton musiqueButton = new JButton();

	public Image partDO = null;

	public Image partNOT = null;

	public Image partOK = null;

	public Image progressFull = null;
	
	public Image progressBar2 = null;
	
	public JProgressBar bar = new JProgressBar();

	public HashMap<String, Float> jars = new HashMap<String, Float>();
	public HashMap<String, Float> mods = new HashMap<String, Float>();
	public HashMap<String, Float> libs = new HashMap<String, Float>();

	public String actualFile = "";
	public String actualThread = "";
	
	public float totalLength = 0;
	
	public float actualTotalLength = 0;

	public DownloadForm(final GlobalForm form)
	{
		setLayout(null);
		this.setFocusable(true);
		this.setOpaque(false);
		this.setBorder(javax.swing.BorderFactory.createEmptyBorder());

		//Image Loading
		try{
			this.partDO = ImageIO.read(DownloadForm.class.getResource("/fr/aurum/Resources/partDO.png"));
			this.partNOT = ImageIO.read(DownloadForm.class.getResource("/fr/aurum/Resources/partNOT.png"));
			this.partOK = ImageIO.read(DownloadForm.class.getResource("/fr/aurum/Resources/partOK.png"));
			this.progressFull = ImageIO.read(DownloadForm.class.getResource("/fr/aurum/Resources/progressFull.png"));
			this.progressBar2 = ImageIO.read(DownloadForm.class.getResource("/fr/aurum/Resources/GreenProgressBar.png"));
		} catch (IOException e) {e.printStackTrace();}

		//Musique Button
		this.musiqueButton.setBounds(805, 193, 45, 45);
		this.musiqueButton.setOpaque(false);
		this.musiqueButton.setFocusable(false);
		this.musiqueButton.setBorderPainted(false);
		this.musiqueButton.setContentAreaFilled(false);
		this.add(musiqueButton);

		this.musiqueButton.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent ae)
			{
				System.out.println(getWidth()+"|"+getHeight());
				form.musique.setVisible(true);
				form.musiqueOpen = true;
				form.musique.musiquePane.preparePlaylist();
			}
		});
	}

	public void addDownload(String file, String type)
	{
		if(type.equals("LIB"))
			libs.put(file, 0.0f);
		else if(type.equals("JAR"))
			jars.put(file, 0.0f);
		else if(type.equals("MOD"))
			mods.put(file, 0.0f);
	}

	public void setActualFile(String file)
	{
		actualFile = file;
	}

	public void update(Graphics g)
	{
		paintComponent(g);
	}

	@Override
	public void paintComponent(Graphics g2)
	{
		super.paintComponent(g2);

		g2.setFont(new Font("Arial", 1, 11));
		g2.setColor(Color.black);

		if(!mods.isEmpty())
		{
			int i = 0;
			for(String s : mods.keySet())
			{
				if(mods.get(s) != 0 && mods.get(s) != 100)
					g2.drawImage(partDO, 330, 64+(26*i), 137, 25, null);
				else if(mods.get(s) == 100)
					g2.drawImage(partOK, 330, 64+(26*i), 137, 25, null);
				else
					g2.drawImage(partNOT, 330, 64+(26*i), 137, 25, null);

				String s2 = s;
				int length = s.length();
				if(length > 15) {s2 = s.substring(0, 15)+"...";}
				
				g2.drawString(s2, 337, 81+(26*i));
				i = i+1;
			}
		}

		if(!libs.isEmpty())
		{
			int i = 0;
			for(String s : libs.keySet())
			{
				int x = 0;
				int y = 0;
				if(i>=13)
				{
					x = 142;
					y = -2+(-12*28);
				}
				
				if(libs.get(s) != 0 && libs.get(s) != 100)
					g2.drawImage(partDO, 29+x, 64+(26*i)+y, 137, 25, null);
				else if(libs.get(s) == 100)
					g2.drawImage(partOK, 29+x, 64+(26*i)+y, 137, 25, null);
				else
					g2.drawImage(partNOT, 29+x, 64+(26*i)+y, 137, 25, null);

				String s2 = s;
				int length = s.length();
				if(length > 15) {s2 = s.substring(0, 16)+"...";}
				
				g2.drawString(s2, 36+x, 81+(26*i)+y);
				i = i+1;
			}
		}

		if(!jars.isEmpty())
		{
			int i = 0;
			for(String s : jars.keySet())
			{
				int x = 0;
				int y = 0;
				if(i+libs.size()>13)
				{
					x = 142;
					y = -13*56;
				}
				if(i+libs.size()>=26)
				{
					x = 284;
					y = -13*56;
				}
				
				if(jars.get(s) != 0 && jars.get(s) != 100)
					g2.drawImage(partDO, 29+x, 64+(26*i)+(28*libs.size())+y, 137, 25, null);
				else if(jars.get(s) == 100)
					g2.drawImage(partOK, 29+x, 64+(26*i)+(28*libs.size())+y, 137, 25, null);
				else
					g2.drawImage(partNOT, 29+x, 64+(26*i)+(28*libs.size())+y, 137, 25, null);

				String s2 = s;
				int length = s.length();
				if(length > 15) {s2 = s.substring(0, 16)+"...";}
				
				g2.drawString(s2, 36+x, 81+(26*i)+(28*libs.size()+y));
				i = i+1;
			}
		}

		if(!this.actualFile.equals(""))
		{
			NumberFormat nf = NumberFormat.getInstance();
			nf.setMaximumFractionDigits(2);
			nf.setMinimumFractionDigits(0);

			float percent = 0.0f;
			float totalPercent = (this.actualTotalLength/this.totalLength)*100;
			
			if(mods.containsKey(actualFile))
				percent = mods.get(actualFile);
			else if(libs.containsKey(actualFile))
				percent = libs.get(actualFile);
			else if(jars.containsKey(actualFile))
				percent = jars.get(actualFile);
			
			int taille_x = (int) ((227/100)*Math.round(percent)*1.15);
			int taille_y = 20;
			int pos_x = 507;
			int pos_y = 333;

			g2.drawImage(progressBar2, 
			          pos_x, pos_y,
			          pos_x + taille_x,   pos_y + taille_y,
			          0, 0, 
			          taille_x,  taille_y, 
			          null);
			
			taille_x = (int) ((227/100)*Math.round(totalPercent)*1.15);
			pos_y = 366;
			
			g2.drawImage(progressFull, 
			          pos_x, pos_y,
			          pos_x + taille_x,   pos_y + taille_y,
			          0, 0, 
			          taille_x,  taille_y, 
			          null);

			g2.drawString(this.actualFile, 510, 347);
			if(mods.containsKey(actualFile))
				g2.drawString( nf.format(percent).split(",")[0]+" %", 745, 347);
			else if(libs.containsKey(actualFile))
				g2.drawString(nf.format(percent).split(",")[0]+" %", 745, 347);
			else if(jars.containsKey(actualFile))
				g2.drawString(nf.format(percent).split(",")[0]+" %", 745, 347);
			g2.drawString(nf.format(totalPercent).split(",")[0]+" %", 745, 380);
		}
		g2.drawString(actualThread, 511, 380);
		g2.dispose();
	}
}