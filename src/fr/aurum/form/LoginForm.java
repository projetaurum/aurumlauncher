package fr.aurum.form;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.swing.ImageIcon;
import javax.swing.JCheckBox;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JProgressBar;
import javax.swing.JTextField;

import net.minecraft.Launcher;
import fr.aurum.MinecraftFrame;
import fr.aurum.auth.AuthManager;
import fr.aurum.objects.AurumButton;
import fr.aurum.objects.AurumModpackButton;
import fr.aurum.objects.NotifiedButton;
import fr.aurum.objects.WebButton;
import fr.aurum.update.AurumUpdater;
import fr.aurum.update.libs.Libraries;
import fr.aurum.utils.FileUtil;


public class LoginForm extends JPanel
{
	private static final long serialVersionUID = 1L;

	private JTextField userName;

	private JPasswordField password;

	private JCheckBox rememberBox = new JCheckBox("Retenir mot de passe");

	private AurumButton launchButton = new AurumButton("connect");

	private AurumButton playButton = new AurumButton("play");

	public AurumModpackButton modpackButton = new AurumModpackButton("modpack");

	private AurumButton decoButton = new AurumButton("disconnect");

	private WebButton web = new WebButton("web", "website");

	private WebButton forum = new WebButton("forum", "forum");

	public NotifiedButton maj = new NotifiedButton("news");

	private ImageIcon unchecked = new ImageIcon(FileUtil.registerImage("unchecked.png"));

	private ImageIcon checked = new ImageIcon(FileUtil.registerImage("checked.png"));

	BufferedImage image = null;

	public AuthManager auth = new AuthManager();

	public boolean isLogged = false;
	public boolean needLogin = false;

	private MinecraftFrame frame;

	public ModpacksForm modpack;

	/**
	 * Ne doit jamais etre modifie en dehors
	 * des tests hors connexion !
	 * Ceci n'est pas un launcher de version crack !
	 */
	private boolean isOffline = false;
	
	private GlobalForm form;

	public LoginForm(final MinecraftFrame launcherFrame, GlobalForm form)
	{
		setLayout(null);
		this.setFocusable(true);
		this.setOpaque(false);
		this.setBorder(javax.swing.BorderFactory.createEmptyBorder());

		this.frame = launcherFrame;
		this.form = form;

		//Website
		this.web.setBounds(627, 50, 175, 30);
		this.add(web);

		//Forum
		this.forum.setBounds(627, 84, 175, 30);
		this.add(forum);

		//maj
		this.maj.setBounds(627, 20, 77,26);
		this.add(maj);

		Thread t = new Thread()
		{
			public void run()
			{
				try 
				{
					if(launcherFrame.auth.loadFromStorage())
					{
						addPlay();
					}
				} catch (IOException e) {e.printStackTrace();}
				
				if(!isLogged)
				{
					initLoginFields();
					needLogin = true;
				}
			}
		};
		t.start();
		this.modpack = new ModpacksForm(this);
		this.modpack.setBounds(17, 17, 589, 392);
		this.add(this.modpack);
	}

	public void addPlay()
	{
		isLogged = true;
		
		//Play Button
		this.playButton.setBounds(630,327,170,30);
		this.add(playButton);

		this.playButton.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e)
			{
				Thread aurumUpdater = new AurumUpdater(new Launcher(),frame,new String[]{"",""},"");
				aurumUpdater.start();
				
				//form.authKeyDialog.setVisible(true);
			}
		});
		
		//ModPack Button
		this.modpackButton.setBounds(630,292,170,30);
		this.add(this.modpackButton);

		this.modpackButton.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e) 
			{
				if(modpackButton.isReverted())
				{
					modpackButton.setReverted(false);
					modpack.removePacks();
				}
				else
				{
					modpackButton.setReverted(true);

					Thread t = new Thread() 
					{
						public void run() 
						{
							modpack.initPacks();
						}
					};
					t.start();
				}

				Thread t = new Thread() 
				{
					public void run() 
					{
						while(true)
						{
							modpack.repaint();
							try {
								Thread.sleep(20L);
							} catch (InterruptedException e) {e.printStackTrace();}
						}
					}
				};
				t.start();
			}
		});

		//Deconnection Button
		this.decoButton.setBounds(629, 392, 170, 30);
		this.add(decoButton);

		this.decoButton.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent ae) 
			{
				removePlay();
				frame.auth.logOut();
				frame.auth.deleteStorage();
			} 
		});
		this.repaint();
	}

	public void removeLogin()
	{
		this.isLogged = true;

		this.remove(this.password);
		this.remove(this.userName);
		this.remove(this.rememberBox);
		this.remove(this.launchButton);
	}

	public void removePlay()
	{
		this.isLogged = false;

		this.remove(playButton);
		this.remove(modpackButton);
		this.remove(decoButton);
		this.repaint();

		initLoginFields();
	}

	public void initLoginFields()
	{
		//UserNameField Init
		this.userName = new JTextField(40);
		this.userName.setBounds(630,287,170,25);
		this.userName.setBackground(new Color(50,54,57));
		this.userName.setForeground(Color.BLACK);
		this.userName.setCaretColor(Color.GRAY);
		this.userName.setFont(new Font("Segoe UI",Font.ITALIC+Font.BOLD, 14));
		this.userName.setBorder(javax.swing.BorderFactory.createEmptyBorder()); //Remove border
		this.add(userName);

		//PasswordField Init
		this.password = new JPasswordField(40);
		this.password.setBounds(630,327,170,25);
		this.password.setBackground(new Color(50,54,57));
		this.password.setForeground(Color.BLACK);
		this.password.setCaretColor(Color.GRAY);
		this.password.setFont(new Font("Segoe UI",Font.ITALIC+Font.BOLD, 20));
		this.password.setBorder(javax.swing.BorderFactory.createEmptyBorder()); //Remove border
		this.password.setFocusable(true);
		this.add(password);

		//Launch Button
		this.launchButton.setBounds(629, 392, 170, 30);
		this.add(launchButton);

		//Remember Me
		this.rememberBox.setBounds(776, 363, 15, 12);
		this.rememberBox.setOpaque(false);
		this.rememberBox.setIcon(unchecked);
		this.rememberBox.setDisabledIcon(unchecked);
		this.rememberBox.setDisabledSelectedIcon(unchecked);
		this.rememberBox.setSelectedIcon(checked);
		this.rememberBox.setDisabledSelectedIcon(unchecked);
		this.add(rememberBox);

		this.launchButton.addActionListener(new ActionListener() 
		{
			@SuppressWarnings("deprecation")
			public void actionPerformed(ActionEvent ae) 
			{
				frame.auth.setUsername(userName.getText());
				frame.auth.setPassword(password.getText());

				try 
				{
					frame.auth.logIn();
				} catch (IOException e1) {e1.printStackTrace();}

				if(rememberBox.isSelected())
				{
					try 
					{
						frame.auth.saveForStorage();
					} catch (IOException e) {e.printStackTrace();}
				}
				removeLogin();
				addPlay();
				//launcherFrame.setSize(860, 480);
				//launcherFrame.setResizable(true);
			} 
		});
	}
	
	public void update(Graphics g)
	{
		paintComponent(g);
	}
}