package fr.aurum.form;

import java.net.URL;

import javax.swing.BorderFactory;
import javax.swing.JEditorPane;
import javax.swing.JPanel;

import fr.aurum.MinecraftFrame;

public class NewsForm extends JPanel
{
	private static final long serialVersionUID = 491618599065604348L;
	
	private JEditorPane news = new JEditorPane();
	
	public NewsForm()
	{
		setLayout(null);
		this.setOpaque(false);
		this.setFocusable(true);
		this.setBorder(javax.swing.BorderFactory.createEmptyBorder());
		
		//News
		this.news.setBounds(23, 27, 808, 365);
		try 
		{
			this.news.setPage(new URL(MinecraftFrame.globalURL+"news.html"));
			System.out.println(MinecraftFrame.globalURL);
		} catch (Exception e) 
		{System.err.println("Impossible d'acc�der � "+MinecraftFrame.globalURL+"/news.html");}
		
		this.news.setOpaque(false);
		this.news.setBorder(BorderFactory.createEmptyBorder());
		this.news.setEditable(false);
		this.add(news);
	}
}