package fr.aurum.form;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Frame;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JButton;
import javax.swing.JPanel;

import fr.aurum.MinecraftFrame;
import fr.aurum.dialogs.KeyDialog;
import fr.aurum.dialogs.MusiqueDialog;
import fr.aurum.objects.AurumButton;
import fr.aurum.update.AutoUpdater;
import fr.aurum.update.libs.Libraries;
import fr.aurum.utils.FileUtil;

public class GlobalForm extends JPanel
{
	private static final long serialVersionUID = -8166332987016772919L;

	private MinecraftFrame launcherFrame;

	private Image bgImage;
	private Image rememberImage;
	private Image selectedPack;

	private AurumButton minimize = new AurumButton("minimize");

	private AurumButton close = new AurumButton("quit");

	public JButton downloadButton = new JButton();

	private JButton newsButton = new JButton();

	private JButton loginButton = new JButton();

	public LoginForm login = null;

	private NewsForm news = null;

	public DownloadForm download = null;

	private int onglet = 0;

	public MusiqueDialog musique = null;

	public boolean musiqueOpen = false;

	private boolean isDownloadLock = false;

	public KeyDialog authKeyDialog;

	/**
	 * Ne contient que le mot de passe d'acces au serveur
	 * de mise a jour, en aucun cas il ne s'agit d'un vol de mot de passe.
	 */
	public String passUpdate;

	public GlobalForm(final MinecraftFrame launcherFrame)
	{
		//Launcher Frame Config
		this.launcherFrame = launcherFrame;

		this.setLayout(null);
		this.setOpaque(false);

		//Declare Forms
		this.login = new LoginForm(this.launcherFrame, this);
		this.news = new NewsForm();
		this.download = new DownloadForm(this);

		//Images Loading
		this.bgImage = FileUtil.registerImage("forms/LoginForm.png");
		this.rememberImage = FileUtil.registerImage("remember.png");

		//Minimize
		this.minimize.setBounds(717, 12, 20, 20);
		this.add(minimize);

		//Stop
		this.close.setBounds(778, 12, 20, 20);
		this.add(close);

		//LoginForm default onglet
		login.setBounds(0, 39, 854, 480-39);
		this.loginButton.setEnabled(false);
		this.add(login);

		//LoginSwitchButton
		this.loginButton.setBounds(25, 14, 190, 25);
		this.loginButton.setOpaque(false);
		this.loginButton.setFocusable(false);
		this.loginButton.setBorderPainted(false);
		this.loginButton.setContentAreaFilled(false);
		this.add(loginButton);

		//NewsSwitchButton
		this.newsButton.setBounds(220, 14, 190, 25);
		this.newsButton.setOpaque(false);
		this.newsButton.setFocusable(false);
		this.newsButton.setBorderPainted(false);
		this.newsButton.setContentAreaFilled(false);
		this.add(newsButton);

		//DownloadSwitchButton
		this.downloadButton.setBounds(415, 14, 190, 25);
		this.downloadButton.setOpaque(false);
		this.downloadButton.setFocusable(false);
		this.downloadButton.setBorderPainted(false);
		this.downloadButton.setContentAreaFilled(false);
		this.add(downloadButton);

		//Musique
		this.musique = new MusiqueDialog(this);
		this.musique.setBounds(0,0,234,480);
		this.musique.setUndecorated(true);
		this.musique.setLocationRelativeTo(this);
		this.musique.setLocation((int)this.musique.getLocation().getX()+530, (int)this.musique.getLocation().getY());
		this.musique.setResizable(false);

		//AuthKeyDialog
		this.authKeyDialog = new KeyDialog(this);
		this.authKeyDialog.setBounds(this.getWidth()/2,this.getHeight()/2,338,128);
		this.authKeyDialog.setUndecorated(true);
		this.authKeyDialog.setLocationRelativeTo(this);
		this.authKeyDialog.setResizable(false);

		if(this.musiqueOpen)
			this.musique.setVisible(true);

		this.loginButton.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent ae)
			{
				if(!isDownloadLock)
					switchOnglet(0);
			}
		});

		this.loginButton.addMouseListener(new MouseListener ()
		{
			@Override
			public void mouseClicked(MouseEvent e) {}

			@Override
			public void mouseEntered(MouseEvent e) 
			{
				if(!isDownloadLock && onglet != 0)
					setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
			}
			@Override
			public void mouseExited(MouseEvent e) 
			{
				setCursor(Cursor.getDefaultCursor());
			}
			@Override
			public void mousePressed(MouseEvent e) {}
			@Override
			public void mouseReleased(MouseEvent e) {}	
		});

		this.newsButton.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent ae)
			{
				if(!isDownloadLock)
					switchOnglet(1);
			}
		});

		this.newsButton.addMouseListener(new MouseListener ()
		{
			@Override
			public void mouseClicked(MouseEvent e) {}

			@Override
			public void mouseEntered(MouseEvent e) 
			{
				if(!isDownloadLock && onglet != 1)
					setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
			}
			@Override
			public void mouseExited(MouseEvent e) 
			{
				setCursor(Cursor.getDefaultCursor());
			}
			@Override
			public void mousePressed(MouseEvent e) {}
			@Override
			public void mouseReleased(MouseEvent e) {}	
		});

		this.downloadButton.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent ae)
			{
				switchOnglet(2);

			}
		});

		this.downloadButton.addMouseListener(new MouseListener ()
		{
			@Override
			public void mouseClicked(MouseEvent e) {}

			@Override
			public void mouseEntered(MouseEvent e) 
			{
				if(!isDownloadLock && onglet != 2)
					setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
			}
			@Override
			public void mouseExited(MouseEvent e) 
			{
				setCursor(Cursor.getDefaultCursor());
			}
			@Override
			public void mousePressed(MouseEvent e) {}
			@Override
			public void mouseReleased(MouseEvent e) {}	
		});

		this.minimize.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent ae) 
			{
				launcherFrame.setState(Frame.ICONIFIED);
			}
		});

		this.close.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent ae) {
				System.exit(0);
			}
		});
	}

	public int getOnglet() 
	{
		return this.onglet;
	}

	public void switchOnglet(int onglet)
	{
		if(this.onglet == 0)
		{
			this.remove(login);
			this.validate();

			this.loginButton.setEnabled(true);
		}
		else if(this.onglet == 1)
		{
			this.remove(news);
			this.validate();
			this.newsButton.setEnabled(true);
		}
		else if(this.onglet == 2)
		{
			this.remove(download);
			this.validate();
			this.downloadButton.setEnabled(true);
		}

		if(onglet == 0)
		{
			this.add(login);
			login.setBounds(0, 39, 854, 480-39);
			this.bgImage = FileUtil.registerImage("forms/LoginForm.png");
			this.repaint();
			this.loginButton.setEnabled(false);
			this.downloadButton.repaint();
		}
		else if(onglet == 1)
		{
			this.add(news);
			news.setBounds(0, 39, 854, 480-39);
			this.bgImage = FileUtil.registerImage("forms/NewsForm.png");
			this.repaint();

			this.newsButton.setEnabled(false);
		}
		else if(onglet == 2)
		{
			this.add(download);
			download.setBounds(0, 39, 854, 480-39);
			this.bgImage = FileUtil.registerImage("forms/DownloadForm.png");
			this.repaint();

			this.downloadButton.setEnabled(false);
		}

		this.setPreferredSize(new Dimension(854, 480));
		this.onglet = onglet;
	}

	public void lockDownload()
	{
		this.isDownloadLock = true;
	}

	public void update(Graphics g)
	{
		paint(g);
	}

	@Override
	public void paintComponent(Graphics g2)
	{
		super.paintComponent(g2);
		g2.drawImage(this.bgImage, 0, 0, 854, 480, this);

		if(this.getOnglet() == 0)
		{
			if(this.login.maj.getNotified()) 
				this.login.maj.drawNotification(g2);

			g2.setColor(Color.black);
			g2.setFont(new Font("Segoe UI",Font.ITALIC, 20));
			g2.drawString(AutoUpdater.versionName, 630, 80);

			if(this.login.needLogin)
				g2.drawImage(this.rememberImage, 643, 404, 120, 9, null);
		}
		g2.setFont(new Font("Arial", 1, 12));
	}
}