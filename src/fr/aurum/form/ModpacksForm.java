package fr.aurum.form;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JPanel;
import javax.swing.JTextField;

import fr.aurum.objects.AurumButton;
import fr.aurum.objects.AurumPackButton;
import fr.aurum.utils.FileUtil;

public class ModpacksForm extends JPanel
{
	private static final long serialVersionUID = -7547625135631432149L;
	public Image bgImage;
	public Image emptyPack;
	public static Image loadingPack;
	public static Image loadingIcon;

	private int rotate;

	private LoginForm form;

	private AurumPackButton[] packs = new AurumPackButton[6];

	private AurumButton valide = new AurumButton("valider");

	private JTextField keyField = new JTextField();

	private String packSelected;
	private int packSelectedIndex = -1;

	private AurumPackButton pack = null;

	public ModpacksForm(LoginForm form)
	{
		setLayout(null);
		this.setBorder(javax.swing.BorderFactory.createEmptyBorder());

		this.form = form;

		if(this.form.modpackButton.isReverted())
			initPacks();
		loadImages();
	}

	public void loadImages()
	{
		this.bgImage = FileUtil.registerImage("forms/ModpackForm.png");
		this.emptyPack = FileUtil.registerImage("packs/empty.png");
		this.loadingPack = FileUtil.registerImage("packs/loading_back.png");
		this.loadingIcon = FileUtil.registerImage("packs/loading.png");
	}

	public void removePacks()
	{
		for(AurumButton button : packs)
		{
			if(button != null)
				this.remove(button);
		}
		this.remove(valide);
		this.remove(keyField);
	}

	public void initPacks()
	{
		this.packs[0] = new AurumPackButton("Production/production");
		this.packs[1] = new AurumPackButton("Architecte/architecte");
		this.packs[2] = new AurumPackButton("Developpeur/developpeur");

		int i = 0;
		for(final AurumButton button : packs)
		{
			if(button != null)
			{
				if(i<3)
					button.setBounds(35+(i*(22+153)), 6, 153, 154);
				else
					button.setBounds(35+(i*(22+153)), 175, 153, 154);

				this.add(button);

				final int index = i;

				button.addActionListener(new ActionListener()
				{
					@Override
					public void actionPerformed(ActionEvent arg0) 
					{
						if(packSelectedIndex != index)
						{
							if(pack != null)
								form.remove(pack);
							pack = new AurumPackButton(button.getType());
							pack.setBounds(627,120,153,154);
							form.add(pack);
							form.repaint();

							packSelected = packs[index].getPackName();
							packSelectedIndex = index;
						}
						else
						{
							form.remove(pack);
							packSelected = "";
							packSelectedIndex = -1;
						}
					}
				});
			}
			i=i+1;
		}

		//Valide Button
		this.valide.setBounds(220,350,121,30);
		this.add(valide);

		//Key Field
		this.keyField.setBounds(37, 351, 173, 28);
		this.keyField.setBackground(new Color(50,54,57));
		this.keyField.setForeground(Color.BLACK);
		this.keyField.setCaretColor(Color.GRAY);
		this.keyField.setFont(new Font("Segoe UI",0, 20));
		this.keyField.setBorder(javax.swing.BorderFactory.createEmptyBorder()); //Remove border
		this.keyField.setFocusable(true);
		this.add(keyField);
	}

	public void update(Graphics g)
	{
		paintComponent(g);
	}

	public void paintComponent(Graphics g2)
	{
		if(this.form.modpackButton.isReverted())
		{
			g2.drawImage(this.bgImage, 0, 0, this.getWidth(), this.getHeight(), null);

			for(int i = 0;i<3;i++)
			{
				if(this.packs[i] == null)
					g2.drawImage(this.emptyPack, 35+(i*(22+153)), 6, 153, 154, null);
			}
			for(int i = 0;i<3;i++)
			{
				if(this.packs[i+3] == null)
					g2.drawImage(this.emptyPack, 35+(i*(22+153)), 175, 153, 154, null);
			}
		}
	}

	public AurumPackButton getPack(int index)
	{
		return this.packs[index];
	}

	public int getSelectedPackIndex()
	{
		return this.packSelectedIndex;
	}

	public String getSelectedPackName()
	{
		return this.packSelected;
	}

	public static final Image getLoadingPackImage()
	{
		return loadingPack;
	}

	public static final Image getLoadingPackIcon()
	{
		return loadingIcon;
	}
}