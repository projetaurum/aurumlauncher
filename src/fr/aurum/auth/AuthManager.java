package fr.aurum.auth;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.net.PasswordAuthentication;
import java.net.Proxy;
import java.net.URL;
import java.util.UUID;

import net.minecraft.Util;
import net.minecraft.launcher.authentification.Agent;
import net.minecraft.launcher.authentification.AuthenticationRequest;
import net.minecraft.launcher.authentification.AuthenticationResponse;
import net.minecraft.launcher.authentification.RefreshRequest;
import net.minecraft.launcher.authentification.RefreshResponse;
import net.minecraft.launcher.authentification.Response;
import net.minecraft.launcher.authentification.UserMigratedException;

import org.apache.commons.lang3.StringUtils;

import com.google.gson.Gson;
import com.google.gson.JsonParseException;

public class AuthManager
{
    private String clientToken = UUID.randomUUID().toString();
    
    private final Proxy proxy = Proxy.NO_PROXY;
    private final PasswordAuthentication proxyAuth = null;
    
    private final Gson gson = new Gson();
	
	private static final URL ROUTE_AUTHENTICATE = Util.constantURL("https://authserver.mojang.com/authenticate");
	private static final URL ROUTE_REFRESH = Util.constantURL("https://authserver.mojang.com/refresh");
	private final Agent agent = Agent.MINECRAFT;
	private String accessToken;
	private boolean isOnline;

	public AuthManager()
	{
		
	}
	
	public boolean canLogIn() 
	{
		return !canPlayOnline() && StringUtils.isNotBlank(getUsername()) && (StringUtils.isNotBlank(getPassword()) || StringUtils.isNotBlank(getAccessToken()));
	}

	public boolean canPlayOnline() 
	{
		return isLoggedIn() && isOnline;
	}
	
	public void setAccessToken(String token)
	{
		this.accessToken = token;
	}

	public String getAccessToken() 
	{
		return accessToken;
	}

	public Agent getAgent() 
	{
		return agent;
	}
	
    public void setClientToken(final String clientToken) 
    {
        this.clientToken = clientToken;
    }

	public String getClientToken() 
	{
		return clientToken;
	}

	public boolean isLoggedIn() 
	{
		return StringUtils.isNotBlank(accessToken);
	}

	/**
	 * Get ClientToken
	 * Get AccessToken
	 * Get Username
	 * From storage file
	 * @throws IOException 
	 *
	 */
	public boolean loadFromStorage() throws IOException 
	{
		//logOut();

		if(!canLoadFromStorage())
			return false;
		
		File auth = new File(Util.getWorkingDirectory()+"/login.aur");
		
		BufferedReader br = new BufferedReader(new FileReader(auth));
		this.username = br.readLine();
		this.accessToken = br.readLine();
		this.clientToken = br.readLine();
		br.close();
		
		return this.logInWithToken();
	}
	
	public void deleteStorage()
	{
		File auth = new File(Util.getWorkingDirectory()+"/login.aur");
		
		if(auth.exists())
		{
			auth.delete();
		}
	}
	
	public boolean canLoadFromStorage()
	{
		return new File(Util.getWorkingDirectory()+"/login.aur").exists();
	}
	
	public void saveForStorage() throws IOException
	{
		File auth = new File(Util.getWorkingDirectory()+"/login.aur");
		
		BufferedWriter bw = new BufferedWriter(new FileWriter(auth));
		bw.write(this.username);
		bw.newLine();
		bw.write(this.accessToken);
		bw.newLine();
		bw.write(this.clientToken);
		bw.close();
	}

	public boolean logIn() throws IOException
	{
		if(StringUtils.isBlank(getUsername()))
		{
			System.err.println("Invalid username");
			return false;
		}

		if(StringUtils.isNotBlank(getAccessToken()))
			return logInWithToken();
		else if(StringUtils.isNotBlank(getPassword()))
			try {
				return logInWithPassword();
			} catch (UserMigratedException e) {e.printStackTrace();}
		else
			System.err.println("Invalid password");
		return false;
	}

	protected boolean logInWithPassword() throws UserMigratedException
	{
		if(StringUtils.isBlank(getUsername()))
		{
			System.err.println("Invalid username");
			return false;
		}
		if(StringUtils.isBlank(getPassword()))
		{
			System.err.println("Invalid password");
			return false;
		}

		System.out.println("Logging in with username & password");

		final AuthenticationRequest request = new AuthenticationRequest(this, getPassword());
		final AuthenticationResponse response = (AuthenticationResponse) makeRequest(ROUTE_AUTHENTICATE, request, AuthenticationResponse.class);

		if(!response.getClientToken().equals(getClientToken()))
		{
			System.err.println("Server requested we change our client token. Don't know how to handle this!");
			return false;
		}
		accessToken = response.getAccessToken();
		return true;
	}

	protected boolean logInWithToken() throws IOException 
	{
		if(StringUtils.isBlank(getUsername()))
		{
			System.err.println("Invalid username");
			return false;
		}
		if(StringUtils.isBlank(getAccessToken()))
		{
			System.err.println("Invalid access token");
			return false;
		}

		System.out.println("Logging in with access token");

		final RefreshRequest request = new RefreshRequest(this);
		RefreshResponse response = null;
		try {
			response = (RefreshResponse) makeRequest(ROUTE_REFRESH, request, RefreshResponse.class);
		} catch (UserMigratedException e) {e.printStackTrace();}

		if(response.getClientToken() != null && getClientToken() != null)
		{
			if(!response.getClientToken().equals(getClientToken()))
			{
				System.err.println("Server requested we change our client token. Don't know how to handle this!");
				return false;
			}
		}
		else
			return false;

		accessToken = response.getAccessToken();
		this.saveForStorage();
		return true;
	}

	public void logOut() 
	{
		accessToken = null;
		isOnline = false;
	}
	
    public PasswordAuthentication getProxyAuth() 
    {
        return proxyAuth;
    }
	
    public Proxy getProxy() 
    {
        return proxy;
    }

	protected <T extends Response> Response makeRequest(final URL url, final Object input, final Class<T> classOfT) throws UserMigratedException 
	{
		try {
			final String jsonResult = Util.performPost(url, gson.toJson(input), this.getProxy(), "application/json", true);
			final Response result = gson.fromJson(jsonResult, classOfT);

			if(result == null)
				return null;

			if(StringUtils.isNotBlank(result.getError())) {
				if("UserMigratedException".equals(result.getCause()))
					throw new UserMigratedException(result.getErrorMessage());
				if(result.getError().equals("ForbiddenOperationException"))
					System.err.println(result.getErrorMessage());
				System.err.println(result.getErrorMessage());
			}

			isOnline = true;

			return result;
		}
		catch(final IOException e) {
			System.err.println("Cannot contact authentication server");
		}
		catch(final IllegalStateException e) {
			System.err.println("Cannot contact authentication server");
		}
		catch(final JsonParseException e) {
			System.err.println("Cannot contact authentication server");
		}
		return null;
	}

	@Override
	public String toString() 
	{
		return "YggdrasilAuthenticationService{agent=" + agent + ", sessionToken='" + getClientToken() + '\'' + ", username='" + getUsername() + '\'' + ", isLoggedIn=" + isLoggedIn() + ", canPlayOnline=" + canPlayOnline() + ", accessToken='" + accessToken + '\'' + ", clientToken='" + getClientToken() + '\'' + '}';
	}
	
	private String username;
	private String password;

	private boolean shouldRememberMe = true;

	protected String getPassword() 
	{
		return password;
	}

	public String getUsername() 
	{
		return username;
	}

	public void setPassword(final String password) 
	{
		if(isLoggedIn() && canPlayOnline() && StringUtils.isNotBlank(password))
			throw new IllegalStateException("Cannot set password whilst logged in & online");

		this.password = password;
	}

	public void setRememberMe(final boolean rememberMe) 
	{
		shouldRememberMe = rememberMe;
	}

	public void setUsername(final String username) 
	{
		if(isLoggedIn() && canPlayOnline())
			throw new IllegalStateException("Cannot change username whilst logged in & online");

		this.username = username;
	}

	public boolean shouldRememberMe() 
	{
		return shouldRememberMe;
	}
}