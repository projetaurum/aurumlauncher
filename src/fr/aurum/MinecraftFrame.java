package fr.aurum;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;

import javax.swing.JFrame;

import net.minecraft.Launcher;
import fr.aurum.auth.AuthManager;
import fr.aurum.form.GlobalForm;
import fr.aurum.form.LoginForm;
import fr.aurum.update.AutoUpdater;
import fr.aurum.update.assets.Assets;
import fr.aurum.update.libs.Libraries;
import fr.aurum.utils.FileUtil;

public class MinecraftFrame extends JFrame
{
	int posX=0,posY=0;

	public static final int VERSION = 12;
	private static final long serialVersionUID = 1L;
	private Launcher launcher;
	public LoginForm loginForm;
	public GlobalForm globalForm;
	public AuthManager auth;

	public static final String globalURL = "http://launcher.projet-aurum.fr/launcher/";
	public static final String GLOBAL_IP = "localhost";

	public MinecraftFrame()
	{	
		super("Minecraft Launcher");

		setUndecorated(true);
		System.out.println("HEY");
		if(AutoUpdater.needUpdate())
		{
			System.out.println("Mise a jour du launcher !");
			new Thread() 
			{
				public void run() 
				{
					try 
					{
						AutoUpdater.updateLauncher();
					} catch (Exception e) {System.err.println("Erreur dans la mise a jour du launcher !");}
				}
			}
			.start();
		}

		this.auth = new AuthManager();

		this.addMouseListener(new MouseAdapter()
		{
			public void mousePressed(MouseEvent e)
			{
				if(e.getY() < 40)
				{
					posX=e.getX();
					posY=e.getY();
				}
			}
		});

		this.addMouseMotionListener(new MouseAdapter()
		{
			public void mouseDragged(MouseEvent evt)
			{
				if(evt.getY() < 40)
				{
					setLocation(evt.getXOnScreen()-posX,evt.getYOnScreen()-posY);
					globalForm.musique.setLocation((evt.getXOnScreen()-posX)+854,evt.getYOnScreen()-posY);
				}
			}
		});

		this.globalForm = new GlobalForm(this);
		setLayout(new BorderLayout());
		
		add("Center",this.globalForm);
		setResizable(false);
		this.globalForm.setPreferredSize(new Dimension(854, 480));
		pack();
		setLocationRelativeTo(null);

		Thread t = new Thread() 
		{
			public void run() 
			{
				while(true)
				{
					globalForm.login.repaint();
					try {
						Thread.sleep(20L);
					} catch (InterruptedException e) {e.printStackTrace();}
				}
			}
		};
		t.start();

		setIconImage(FileUtil.registerImage("favicon.png"));

		addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent arg0) {
				new Thread() {
					public void run() {
						try {
							Thread.sleep(30000L);
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
						System.out.println("FORCING EXIT!");
						System.exit(0);
					}
				}
				.start();
				if (MinecraftFrame.this.launcher != null) {
					MinecraftFrame.this.launcher.stop();
					MinecraftFrame.this.launcher.destroy();
				}
				System.exit(0);
			} } );
	}

	public static void main(String[] args) 
	{
		MinecraftFrame launcherFrame = new MinecraftFrame();
		launcherFrame.setVisible(true);
	}
}