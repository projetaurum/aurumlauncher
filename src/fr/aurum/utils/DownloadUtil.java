package fr.aurum.utils;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;

import fr.aurum.form.DownloadForm;

public class DownloadUtil implements Runnable
{
	private DownloadForm dwl;
	private String chemin;
	private String host;
	private String categorie;
	
	public DownloadUtil(String host, String chemin, DownloadForm dwl, String categorie)
	{
		this.dwl = dwl;
		this.chemin = chemin;
		this.host = host;
		this.categorie = categorie;
	}
	
    public static void getFile(String host, String chemin)
    {
        InputStream input = null;
        FileOutputStream writeFile = null;
        try
        {
            URL url = new URL(host);
            URLConnection connection = url.openConnection();
            input = connection.getInputStream();
            
            String fileName = url.getFile().substring(url.getFile().lastIndexOf('/') + 1);
            writeFile = new FileOutputStream(chemin+fileName);
            byte[] buffer = new byte[1024];
            int read;
            while ((read = input.read(buffer)) > 0)
                writeFile.write(buffer, 0, read);
            writeFile.flush();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        finally
        {
            try
            {
                writeFile.close();
                input.close();
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
        }
    }

	@Override
	public void run() 
	{
        InputStream input = null;
        FileOutputStream writeFile = null;
        try
        {
            URL url = new URL(host);
            URLConnection connection = url.openConnection();
            input = connection.getInputStream();
            
            float fileLength = connection.getContentLength();
            float actLength = 0.0F;
            
            float lastLength = 0;
            
            String fileName = url.getFile().substring(url.getFile().lastIndexOf('/') + 1);
            writeFile = new FileOutputStream(chemin+fileName);
            byte[] buffer = new byte[1024];
            int read;
            while ((read = input.read(buffer)) > 0)
            {
                writeFile.write(buffer, 0, read);
                actLength = actLength+read;
                
                if(categorie.equals("MOD"))
                	dwl.mods.put(fileName, (actLength/fileLength)*100);
                else if(categorie.equals("LIB"))
                	dwl.libs.put(fileName, (actLength/fileLength)*100);
                else
                	dwl.jars.put(fileName, (actLength/fileLength)*100);
                
                dwl.actualTotalLength = dwl.actualTotalLength+(actLength-lastLength);
                
                lastLength = actLength;
            }
            writeFile.flush();
            if(categorie.equals("MOD"))
            	dwl.mods.put(fileName, 100.0f);
            else if(categorie.equals("LIB"))
            	dwl.libs.put(fileName, 100.0f);
            else
            	dwl.jars.put(fileName, 100.0f);
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        finally
        {
            try
            {
                writeFile.close();
                input.close();
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
        }
	}
	
	public static final Float getLength(String host)
	{
		try 
		{
        URL url = new URL(host);
        URLConnection connection = url.openConnection();
        return (float) connection.getContentLength();
		} catch (Exception e) {e.printStackTrace();}
		return null;
	}
}