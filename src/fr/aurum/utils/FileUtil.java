package fr.aurum.utils;

import java.awt.Image;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;

import javax.imageio.ImageIO;

import fr.aurum.form.GlobalForm;

public class FileUtil 
{
	public static final boolean move(File source,File destination) 
	{
		if(!destination.exists()) 
		{
			boolean result = source.renameTo(destination);
			if(!result ) 
			{
				result = true;
				result &= copyFile(source,destination);
				if(result) result &= source.delete();

			} 
			return (result);
		} 
		else
		{
			destination.delete();
			boolean result = source.renameTo(destination);
			if(!result ) 
			{
				result = true;
				result &= copyFile(source,destination);
				if(result) result &= source.delete();

			} 
			return (result);
		}
	} 

	public static boolean copyFile(File source, File dest)
	{
		try
		{
			java.io.FileInputStream sourceFile = new java.io.FileInputStream(source);
			try
			{
				java.io.FileOutputStream destinationFile = null;
				try
				{
					destinationFile = new FileOutputStream(dest);

					byte buffer[] = new byte[512 * 1024];
					int nbLecture;

					while ((nbLecture = sourceFile.read(buffer)) != -1){
						destinationFile.write(buffer, 0, nbLecture);
					}
				} finally {
					destinationFile.close();
				}
			} finally {
				sourceFile.close();
			}
		} catch (IOException e){
			e.printStackTrace();
			return false;
		}

		return true; 
	}

	public static Image registerImage(String name)
	{
		try {
			return ImageIO.read(GlobalForm.class.getResource("/fr/aurum/Resources/" + name));
		} catch (Exception e) {
			System.err.println("Impossible de lire l'image : "+name+ " !");
		}
		return null;
	}
	
	public static Image registerImage(URL name)
	{
		try 
		{
			return ImageIO.read(name);
		} catch (IOException e) {e.printStackTrace();}
		return null;
	}

	public static void launchWebAdress(String adress) 
	{
		Runtime r = Runtime.getRuntime();
		String osName = System.getProperty("os.name").toLowerCase();
		try {
			if(osName.contains("win"))
				r.exec("cmd /c start " + adress);
			else if(osName.contains("mac"))
				r.exec("open " + adress);
			else if (osName.contains("nix") || osName.contains("nux")) {

				String[] browsers = { "epiphany", "firefox", "mozilla",
						"konqueror", "netscape", "opera", "links", "lynx", "chrome", "google" };

				StringBuffer cmd = new StringBuffer();
				for (int i = 0; i < browsers.length; i++)
					cmd.append((i == 0 ? "" : " || ") + browsers[i] + " \"" + adress + "\" ");

				r.exec(new String[] { "sh", "-c", cmd.toString() });
			} 
			else
				return;
		} catch (IOException e) {
		}
	}
}
