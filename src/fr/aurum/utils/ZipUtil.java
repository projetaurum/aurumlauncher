package fr.aurum.utils;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Enumeration;
import java.util.Set;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.jar.JarOutputStream;
import java.util.jar.Manifest;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

public class ZipUtil 
{
	@SuppressWarnings({ "finally" })
	public static final ArrayList<String> getFiles(String archive) 
	{
		ArrayList<String> fileList = new ArrayList<String>();
		ZipInputStream zipInputStream = null;
		ZipEntry zipEntry = null;
		Long size;

		try {
			zipInputStream = new ZipInputStream(new FileInputStream(archive));
			zipEntry = zipInputStream.getNextEntry();
			while (zipEntry != null) {
				String[] file = new String[3];
				file[0] = zipEntry.getName();
				size = zipEntry.getSize()/1024;
				file[1] = size.toString()+ " ko";
				SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm");
				file[2] = simpleDateFormat.format(new Date(zipEntry.getTime()));

				for(String str : file)
				{
					fileList.add(str);
				}
				zipEntry = zipInputStream.getNextEntry();
			}
		} catch (FileNotFoundException ex) {
			ex.printStackTrace();
		} finally {
			try {
				zipInputStream.close();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
			return fileList;
		}
	}

	@SuppressWarnings("resource")
	public static final void extractTo(String archive, String file, String destPath) 
			throws FileNotFoundException, IOException 
			{
		ZipInputStream zipInputStream = null;
		ZipEntry zipEntry = null;
		byte[] buffer = new byte[2048];

		zipInputStream = new ZipInputStream(new FileInputStream(archive));
		zipEntry = zipInputStream.getNextEntry();
		while (zipEntry != null) {
			if (zipEntry.getName().equalsIgnoreCase(file)) {
				FileOutputStream fileoutputstream = new FileOutputStream(destPath + file);
				int n;

				while ((n = zipInputStream.read(buffer, 0, 2048)) > -1) {
					fileoutputstream.write(buffer, 0, n);
				}

				fileoutputstream.close();
				zipInputStream.closeEntry();

			}
			zipEntry = zipInputStream.getNextEntry();
		}
			}

	public static final void extractAllTo(String archive, String destPath) 
			throws FileNotFoundException, IOException
			{
		ArrayList<String> files = (ArrayList<String>) getFiles(archive);

		for(String file : files)
		{
			extractTo(archive, file, destPath);
		}
			}

	@SuppressWarnings("rawtypes")
	public static final void extractArchive(String archive) throws IOException
	{
		final File file = new File(archive);
		final ZipFile zipFile = new ZipFile(file);
		final byte[] buffer = new byte[2048];
		final File tmpDir = new File(System.getProperty("java.io.tmpdir"), zipFile.getName());

		/*if(!tmpDir.mkdir() && tmpDir.exists()) {
			System.err.println("Cannot create: " + tmpDir);
			System.exit(0);
		}*/

		for(final Enumeration entries = zipFile.entries(); entries.hasMoreElements();) {
			final ZipEntry zipEntry = (ZipEntry) entries.nextElement();
			System.out.println("Unzipping: " + zipEntry.getName());
			final InputStream is = zipFile.getInputStream(zipEntry);
			final File fileToWrite = new File(tmpDir, zipEntry.getName());
			final File folder = fileToWrite.getParentFile();
			/*if(!folder.mkdirs() && !folder.exists()) {
				System.err.println("Cannot create: " + folder);
				System.exit(0);
			}*/

			if(!zipEntry.isDirectory()) {

				final FileOutputStream fos = new FileOutputStream(fileToWrite);
				int size;
				while ((size = is.read(buffer)) != -1) {
					fos.write(buffer, 0, size);
				}
				fos.close();
				is.close();
			}
		}
		zipFile.close();
	}

	public static void unzip(File zipfile, File folder) throws FileNotFoundException, IOException{

		// cr�ation de la ZipInputStream qui va servir � lire les donn�es du fichier zip
		ZipInputStream zis = new ZipInputStream(
				new BufferedInputStream(
						new FileInputStream(zipfile.getCanonicalFile())));

		// extractions des entr�es du fichiers zip (i.e. le contenu du zip)
		ZipEntry ze = null;
		try {
			while((ze = zis.getNextEntry()) != null){

				// Pour chaque entr�e, on cr�e un fichier
				// dans le r�pertoire de sortie "folder"
				File f = new File(folder.getCanonicalPath(), ze.getName());

				// Si l'entr�e est un r�pertoire,
				// on le cr�e dans le r�pertoire de sortie
				// et on passe � l'entr�e suivante (continue)
				if (ze.isDirectory()) {
					f.mkdirs();
					continue;
				}

				// L'entr�e est un fichier, on cr�e une OutputStream
				// pour �crire le contenu du nouveau fichier
				f.getParentFile().mkdirs();
				OutputStream fos = new BufferedOutputStream(
						new FileOutputStream(f));

				// On �crit le contenu du nouveau fichier
				// qu'on lit � partir de la ZipInputStream
				// au moyen d'un buffer (byte[])
				try {
					try {
						final byte[] buf = new byte[8192];
						int bytesRead;
						while (-1 != (bytesRead = zis.read(buf)))
							fos.write(buf, 0, bytesRead);
					}
					finally {
						fos.close();
					}
				}
				catch (final IOException ioe) {
					// en cas d'erreur on efface le fichier
					f.delete();
					throw ioe;
				}
			}
		}
		finally {
			// fermeture de la ZipInputStream
			zis.close();
		}
	}


	public static int BUFFER_SIZE = 10240;
	public static final void createJarArchive(File archiveFile, ArrayList<File> tobeJared) 
	{
		try {
			byte buffer[] = new byte[BUFFER_SIZE];
			// Open archive file
			FileOutputStream stream = new FileOutputStream(archiveFile);
			JarOutputStream out = new JarOutputStream(stream, new Manifest());

			for (int i = 0; i < tobeJared.size(); i++) {
				if (tobeJared.get(i) == null || !tobeJared.get(i).exists())
					continue; // Just in case...

				if(tobeJared.get(i).isDirectory())
				{
					for(File f : tobeJared.get(i).listFiles())
					{
						while(!f.isDirectory())
						{

						}
						tobeJared.add(f);
					}
				}
				System.out.println("Adding " + tobeJared.get(i).getName());



				// Add archive entry
				JarEntry jarAdd = new JarEntry(tobeJared.get(i).getName());
				jarAdd.setTime(tobeJared.get(i).lastModified());


				out.putNextEntry(jarAdd);



				// Write file to archive
				FileInputStream in = new FileInputStream(tobeJared.get(i));
				while (true) {
					int nRead = in.read(buffer, 0, buffer.length);
					if (nRead <= 0)
						break;
					out.write(buffer, 0, nRead);
				}
				in.close();
			}

			out.close();
			stream.close();
			System.out.println("Adding completed OK");
		} catch (Exception ex) {
			ex.printStackTrace();
			System.out.println("Error: " + ex.getMessage());
		}
	}


	public static void createJarArchive( File archiveFile, File root, ArrayList<File> classToJar )
	{
		try
		{
			byte buffer[] = new byte[BUFFER_SIZE];
			// Open archive file
			FileOutputStream stream = new FileOutputStream( archiveFile );
			JarOutputStream out = new JarOutputStream( stream, new Manifest() );

			int i = 0;
			
			while(!classToJar.isEmpty())
			{
				System.out.println(i);
				if( classToJar.get(i) == null || !classToJar.get(i).exists() )
				{
					classToJar.remove(i);
					continue; // Just in case...
				}

				if(classToJar.get(i).isDirectory())
				{
					for(File f : classToJar.get(i).listFiles())
					{
						classToJar.add(f);
					}

					classToJar.remove(i);
					continue;
				}

				// Add archive entry
				String jarName = classToJar.get(i).isDirectory() ? classToJar.get(i).getAbsolutePath() + "/" : classToJar.get(i)
						.getAbsolutePath();


				jarName = jarName.replace( root.getAbsolutePath(), "" ).substring( 1 );
				jarName = jarName.replace( File.separatorChar, '/' );

				/*if(getFiles(archiveFile.getAbsolutePath()).contains(jarName))
                	continue;*/


				JarEntry jarAdd = new JarEntry( jarName );
				System.out.println( "Adding " + jarName );
				jarAdd.setTime( classToJar.get(i).lastModified() );
				out.putNextEntry( jarAdd );


				if( jarAdd.isDirectory() )
				{
					//classToJar.remove(i);
					continue;
				}


				// Write file to archive
				FileInputStream in = new FileInputStream( classToJar.get(i));
				classToJar.remove(i);
				while( true )
				{
					int nRead = in.read( buffer, 0, buffer.length );
					if( nRead <= 0 )
						break;
					out.write( buffer, 0, nRead );
				}
				in.close();


				i++;
			}

			out.close();
			stream.close();
		}
		catch( Exception ex )
		{
			ex.printStackTrace();
		}
	}
	
	public static void jarDir(String directory, String zipName) throws IOException {
        // create a ZipOutputStream to zip the data to
        JarOutputStream zos = new JarOutputStream(new BufferedOutputStream(new FileOutputStream(zipName)));
        String path = "";
        File manFile = new File(directory, JarFile.MANIFEST_NAME);
        if (manFile.exists()) {
            byte[] readBuffer = new byte[8192];
            FileInputStream fis = new FileInputStream(manFile);
            try {
                ZipEntry anEntry = new ZipEntry(JarFile.MANIFEST_NAME);
                zos.putNextEntry(anEntry);
                int bytesIn = fis.read(readBuffer);
                while (bytesIn != -1) {
                    zos.write(readBuffer, 0, bytesIn);
                    bytesIn = fis.read(readBuffer);
                }
            } finally {
                fis.close();
            }
            zos.closeEntry();
        }
        zipDir(directory, zos, path, Collections.singleton(JarFile.MANIFEST_NAME));
        // close the stream
        zos.close();
    }
	
	public static void zipDir(String directory, ZipOutputStream zos, String path, Set<String> exclusions) throws IOException {
        File zipDir = new File(directory);
        // get a listing of the directory content
        String[] dirList = zipDir.list();
        byte[] readBuffer = new byte[8192];
        int bytesIn = 0;
        // loop through dirList, and zip the files
        for (int i = 0; i < dirList.length; i++) {
            File f = new File(zipDir, dirList[i]);
            if (f.isDirectory()) {
                String filePath = f.getPath();
                zipDir(filePath, zos, path + f.getName() + "/", exclusions);
                continue;
            }
            String entry = path + f.getName();
            if (!exclusions.contains(entry)) {
                FileInputStream fis = new FileInputStream(f);
                try {
                    ZipEntry anEntry = new ZipEntry(entry);
                    zos.putNextEntry(anEntry);
                    bytesIn = fis.read(readBuffer);
                    while (bytesIn != -1) {
                        zos.write(readBuffer, 0, bytesIn);
                        bytesIn = fis.read(readBuffer);
                    }
                } finally {
                    fis.close();
                }
            }
        }
    }
}