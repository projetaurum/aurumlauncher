package fr.aurum.utils;

import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;

import fr.aurum.objects.NotificationFen;

public class GraphicsUtil 
{
	public static void drawNotification(Graphics g, NotificationFen fen) 
	{
		Graphics2D g2d = (Graphics2D)g;
		Font f = new Font("Segeoe UI", 12, 12);
	
		g2d.setColor(Color.white);
		g2d.setFont(f);
		
		int i = 0;
		
		for(String s : fen.getText())
		{
			g2d.drawString(s, fen.getXCoord(), fen.getYCoord()+(15*i));
			i = i+1;
		}
		g2d.setColor(Color.black);
		
		g2d.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER,0.6f));
		
		FontMetrics metrics = g.getFontMetrics(); 
		int size = metrics.stringWidth(fen.getText(0));
		
		g2d.fillRoundRect(fen.getXCoord()-12, fen.getYCoord()-15, size+20, fen.getText().length*18, 4, 4);
		g2d.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER,1.0f));
	}
}