package fr.aurum.objects;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JButton;

import fr.aurum.utils.FileUtil;

public class AurumButton extends JButton implements MouseListener
{
	private static final long serialVersionUID = 1L;
	
	public Image texture = null;
	public Image texture_entered = null;
	
	protected boolean isEntered;
	
	protected String type;
	
	public AurumButton(String type) 
	{
		this.type = type;
		this.isEntered = false;
		this.addMouseListener(this);
		this.setBorderPainted(false);
		this.setContentAreaFilled(false);
		this.setFocusable(true);
		
		initTextures();
	}
	
	public void initTextures()
	{
		texture = FileUtil.registerImage("buttons/"+this.type + ".png");
		texture_entered = FileUtil.registerImage("buttons/"+this.type + "_entered.png");
	}
	
	public void paintComponent(Graphics g) 
	{
		Graphics2D g2d = (Graphics2D)g;
		
		g2d.drawImage(getTexture(), 0, 0, this.getWidth(), this.getHeight(), this);
	}
	
	public Image getTexture()
	{
		if(this.isEntered)
			return this.texture_entered;
		return this.texture;
	}

	@Override
	public void mouseClicked(MouseEvent arg0) {}

	@Override
	public void mouseEntered(MouseEvent arg0) 
	{
		this.isEntered = true;
	}

	@Override
	public void mouseExited(MouseEvent arg0) 
	{
		this.isEntered = false;
	}

	@Override
	public void mousePressed(MouseEvent arg0) 
	{
		this.isEntered = false;
	}

	@Override
	public void mouseReleased(MouseEvent arg0) 
	{
		this.isEntered = false;
	}
	
	public String getType()
	{
		return this.type;
	}
}