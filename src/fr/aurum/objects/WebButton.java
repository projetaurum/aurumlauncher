package fr.aurum.objects;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import fr.aurum.utils.FileUtil;

public class WebButton extends AurumButton 
{
	private static final long serialVersionUID = 1L;

	private final String buttonType;
	
	public WebButton(String type, final String buttonType) 
	{
		super(type);
		this.buttonType = buttonType;
		this.addActionListener(new ActionListener() 
		{
			
			public void actionPerformed(ActionEvent ae) 
			{
				
				if(buttonType.equals("website")) 
					FileUtil.launchWebAdress("http://www.projet-aurum.fr/");
				else 
					FileUtil.launchWebAdress("http://www.projet-aurum.fr/forum/");
			}
			
		});
	}

}
