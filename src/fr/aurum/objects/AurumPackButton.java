package fr.aurum.objects;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.net.MalformedURLException;
import java.net.URL;

import fr.aurum.MinecraftFrame;
import fr.aurum.form.ModpacksForm;
import fr.aurum.utils.FileUtil;

public class AurumPackButton extends AurumButton
{
	private static final long serialVersionUID = 7482358991856141827L;

	private String packName = "";
	
	private int rotate = 0;
	
	public AurumPackButton(String type) 
	{
		super(type);
		
		this.packName = type.split("/")[0];
		
		Thread t = new Thread()
		{
			public void run() 
			{
				while(texture == null || texture_entered == null)
				{
					repaint();
					try {
						Thread.sleep(20L);
					} catch (InterruptedException e) {e.printStackTrace();}
				}
			}
		};
		t.start();
	}
	
	public void update(Graphics g)
	{
		paintComponent(g);
	}
	
	@Override
	public void paintComponent(Graphics g) 
	{
		boolean hasRotate = false;
		
		Graphics2D g2d = (Graphics2D)g;
		
		if(this.texture == null || this.texture_entered == null)
		{
			g2d.drawImage(ModpacksForm.getLoadingPackImage(), 0, 0, 153, 154, null);
			
			double locationX = 53;
			double locationY = 59;

			g2d.rotate(Math.toRadians(-rotate), locationX+(53/2), locationY+(53/2));
			g2d.drawImage(ModpacksForm.getLoadingPackIcon(), (int)locationX, (int)locationY, 53, 53, null);
			g2d.rotate(Math.toRadians(rotate), locationX+(53/2), locationY+(53/2));
			
			hasRotate = true;
		}
		else
			g2d.drawImage(getTexture(), 0, 0, this.getWidth(), this.getHeight(), this);
		
		if(hasRotate)
		{
			if(rotate == 360)
				rotate = 0;
			else
				rotate = rotate+6;
		}
	}
	
	@Override
	public void initTextures()
	{
		Thread t = new Thread() 
		{
			public void run() 
			{
				try 
				{
					texture = FileUtil.registerImage(new URL(MinecraftFrame.globalURL+"packs/"+type+".png"));
					texture_entered = FileUtil.registerImage(new URL(MinecraftFrame.globalURL+"packs/"+type+"_entered.png"));
				} catch (MalformedURLException e) {e.printStackTrace();}
			}
		};
		t.start();
	}
	
	public String getPackName()
	{
		return this.packName;
	}
	
	public String getType()
	{
		return this.type;
	}
}