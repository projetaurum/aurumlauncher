package fr.aurum.objects;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JButton;

import fr.aurum.update.AutoUpdater;
import fr.aurum.utils.FileUtil;
import fr.aurum.utils.GraphicsUtil;

public class NotifiedButton extends JButton implements MouseListener
{
	private static final long serialVersionUID = 1L;
	private String type;
	
	private Image texture;
	private boolean notified;
	private NotificationFen notif;
	
	public NotifiedButton(String type) 
	{
		this.type = type;
		texture = FileUtil.registerImage("buttons/"+this.type + ".png");
		this.addMouseListener(this);
		this.setBorderPainted(false);
		this.setContentAreaFilled(false);
	}
	
	public boolean getNotified() 
	{
		return notified;
	}
	
	public void drawNotification(Graphics g) 
	{
		GraphicsUtil.drawNotification(g, notif);
	}
	
	public void paintComponent(Graphics g) 
	{
		super.paintComponent(g);
		Graphics2D g2d = (Graphics2D)g;
		g2d.drawImage(texture, 0, 0, this.texture.getWidth(this), this.texture.getHeight(this), this);
	}
	
	@Override
	public void mouseClicked(MouseEvent e) {}

	@Override
	public void mouseEntered(MouseEvent arg0) 
	{
		texture = FileUtil.registerImage("buttons/"+this.type + "_entered.png");
		this.notified = true;
		this.notif = new NotificationFen(new String[]{"Nom: "+AutoUpdater.versionName,"Num�ro: "+AutoUpdater.versionID}, this.getBounds().x+100, this.getBounds().y+10+37);
	}

	@Override
	public void mouseExited(MouseEvent arg0) 
	{
		texture = FileUtil.registerImage("buttons/"+this.type + ".png");
		this.notified = false;
		this.notif = null;
	}

	@Override
	public void mousePressed(MouseEvent arg0) {}

	@Override
	public void mouseReleased(MouseEvent arg0) {}
}