package fr.aurum.objects;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JButton;

import fr.aurum.form.GlobalForm;
import fr.aurum.utils.FileUtil;

public class OngletButton extends JButton implements MouseListener{
	
	private static final long serialVersionUID = 1L;
	private boolean activated;
	private Image texture;
	private String type;
	private GlobalForm form;
	private int index;
	private boolean entered = false;
	public OngletButton(String type, GlobalForm form, int index) {
		this.type = type;
		this.texture = FileUtil.registerImage(type + ".png");
		this.activated = false;
		this.form = form;
		this.index = index;

		this.addMouseListener(this);
		this.setBorderPainted(false);
		this.setContentAreaFilled(false);
		this.setFocusable(true);
	}
	
	public void paintComponent(Graphics g) {
		
		if(form.getOnglet() == index)
			activated = true;
		else
			activated = false;
		
		Graphics2D g2d = (Graphics2D)g;
		if(activated)
			this.texture = FileUtil.registerImage(type + "_activated.png");
		else if (entered)
			this.texture = FileUtil.registerImage(this.type + "_entered.png");
		else
			this.texture = FileUtil.registerImage(this.type + ".png");
		
		g2d.drawImage(texture, 0, 0, this.getWidth(), this.getHeight(), this);
		
		}
	
	public void setActivated(boolean b) {
		this.activated = b;
	}
	
	public boolean isActivated() {
		return this.activated;
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		//useless
	}

	@Override
	public void mouseEntered(MouseEvent arg0) {
		if(!activated)
			entered = true;
		else
			return;
	}

	@Override
	public void mouseExited(MouseEvent arg0) {
		if(!activated)
			entered = false;
		else
			return;
		
	}

	@Override
	public void mousePressed(MouseEvent arg0) {
		if(!activated)
			entered = false;
		else
			return;
		
	}

	@Override
	public void mouseReleased(MouseEvent arg0) {
		if(!activated)
			entered = false;
		else
			return; 
		  
	}

}
