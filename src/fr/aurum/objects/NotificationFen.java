package fr.aurum.objects;


public class NotificationFen 
{
	private String[] text;
	private boolean needThread;
	private int xCoord,yCoord;
	
	public NotificationFen(String[] text, int x, int y)
	{
		this.text = text;
		this.needThread = true;
		this.xCoord = x;
		this.yCoord = y;
	}
	
	public String getText(int i)
	{
		return text[i];
	}
	
	public String[] getText()
	{
		return this.text;
	}
	
	public boolean needThread()
	{
		return this.needThread;
	}
	
	public int getXCoord()
	{
		return this.xCoord;
	}
	
	public int getYCoord()
	{
		return this.yCoord;
	}
}