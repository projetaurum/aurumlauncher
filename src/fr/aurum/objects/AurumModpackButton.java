package fr.aurum.objects;

import java.awt.Image;

import fr.aurum.utils.FileUtil;

public class AurumModpackButton extends AurumButton
{
	private boolean isReverted;
	
	private Image texture_entered_reverted;
	private Image texture_reverted;
	
	public AurumModpackButton(String type) 
	{
		super(type);
		
		texture_reverted = FileUtil.registerImage("buttons/"+this.type + "_reverted.png");
		texture_entered_reverted = FileUtil.registerImage("buttons/"+this.type + "_reverted_entered.png");
		
		this.isReverted = false;
	}
	
	public boolean isReverted()
	{
		return this.isReverted;
	}
	
	public void setReverted(boolean b)
	{
		this.isReverted = b;
	}
	
	@Override
	public Image getTexture()
	{
		if(this.isReverted)
		{
			if(this.isEntered)
				return this.texture_entered_reverted;
			return this.texture_reverted;
		}
		return super.getTexture();
	}
}